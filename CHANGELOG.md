# Changelog

## [Unreleased]
### Added
- Add `CHANGELOG.md`
- Add GPG key refresh
- Add Voynich Manuscript "translation" option
- Add JS warning to wrapper template

### Changed
- Download container logs as attachment with friendly name
- CSS/template/code cleanup
- Update translation
- Bind nginx to localhost
- Run ddclient in daemon mode
- Stop redis container during snapshot for backups
- Update GPG pubkeys
- Update python dependencies

### Fixed
- Fix insertion of `SELF_HOSTED` and `TESTING` in settings.env

## [20190424.0] - 2019-04-24
### Changed
- Convert container log json to plaintext on download
- Remove otp requirement for 'Show Log' links

[Unreleased]: https://gitlab.com/gibberfish/daygate/compare/20190424.0...staging
[20190424.0]: https://gitlab.com/gibberfish/daygate/compare/20190423.0...20190424.0
