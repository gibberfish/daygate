Daygate is a web application designed to automate the deployment of the
[Gibberfish](https://gitlab.com/gibberfish/pancrypticon) cloud platform.

It provides an easy to use interface for non-technical users to administer their
cloud services.

## Features include:
  ### 🔐 Two-factor Authentication (2FA)
    • Secures your account with TOTP tokens
  ### 🏗️ Automated Deployment
    • Deploys an encrypted cloud with a single click!
  ### 📛 Dynamic DNS
    • Automatically updates DNS when your IP changes  
  ### 👍 Automatic Updates
    • Get security and feature enhancements automatically
  ### 💾 Backups
    • Back-up your data to Windows or Linux servers
  ### 🔌 Power Control
    • Remote reboot from anywhere in the world
  ### 🐚 Shell Access
    • Secure encrypted shell access via SSH
  ### ☢️ The Nuclear Option
    • Aggressively destroys all of your data on command
  ### 🕶️ Stealth Mode
    • Sends all requests for Gibberfish assets through Tor

Find us on [Matrix](matrix.org) at #gibberfish:matrix.gibberfish.org
