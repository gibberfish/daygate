from django.conf import global_settings, locale

# Add info for Chilean spanish as Django lacks it

gettext_noop = lambda s: s

locale.LANG_INFO["es-cl"] = {
    "bidi": False,
    "code": "es-cl",
    "name": "Chilean Spanish",
    "name_local": "español de Chile",
}
global_settings.LANGUAGES.append(("es-cl", gettext_noop("Chilean Spanish")))

locale.LANG_INFO["es-mx"] = {
    "bidi": False,
    "code": "es-mx",
    "name": "Mexican Spanish",
    "name_local": "español mexicano",
}
global_settings.LANGUAGES.append(("es-mx", gettext_noop("Mexican Spanish")))

locale.LANG_INFO["ms"] = {
    "bidi": False,
    "code": "ms",
    "name": "Malay",
    "name_local": "Melayu",
}
global_settings.LANGUAGES.append(("ms", gettext_noop("Malay")))

locale.LANG_INFO["vy"] = {
    "bidi": False,
    "code": "vy",
    "name": "Voynich",
    "name_local": "Voynich",
}
global_settings.LANGUAGES.append(("vy", gettext_noop("Voynich")))

# LANGUAGES_BIDI = global_settings.LANGUAGES_BIDI + (["es-cl", "es-mx", "ms", "vy"])
