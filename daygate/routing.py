from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.security.websocket import AllowedHostsOriginValidator
from deploy.middleware.websocket_auth import WebsocketAuthMiddlewareStack
import deploy.routing

application = ProtocolTypeRouter(
    {
        "websocket": AllowedHostsOriginValidator(
            WebsocketAuthMiddlewareStack(
                URLRouter(deploy.routing.websocket_urlpatterns)
            )
        )
    }
)
