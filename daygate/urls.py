from django.urls import path, include

urlpatterns = [
    path(r"", include("deploy.urls")),
    path(r"i18n/", include("django.conf.urls.i18n")),
]
