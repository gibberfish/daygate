# -*- coding: utf-8 -*-


from django.apps import AppConfig


class DeployConfig(AppConfig):
    name = "deploy"
