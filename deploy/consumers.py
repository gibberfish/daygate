import re
from channels.generic.websocket import WebsocketConsumer
from asgiref.sync import async_to_sync


class DeployConsumer(WebsocketConsumer):
    """
    Handles websocket client connections
    """

    def connect(self):
        username = "{}".format(self.scope["user"])
        # accept users authenticated by session or by token param
        if not re.match("AnonymousUser", username) or self.scope["auth_token"]:
            # join channel to deploy group
            async_to_sync(self.channel_layer.group_add)("deploy", self.channel_name)
            self.accept()

    def disconnect(self, close_code):
        # remove channel from deploy group
        async_to_sync(self.channel_layer.group_discard)("deploy", self.channel_name)

    def receive(self, text_data):
        # Send message to deploy group
        async_to_sync(self.channel_layer.group_send)(
            "deploy", {"type": "deploy_message", "message": text_data}
        )

    # Receive message from deploy group
    def deploy_message(self, event):
        # Send message to WebSocket
        self.send(text_data=event["message"])
