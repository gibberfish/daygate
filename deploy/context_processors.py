from django_otp.plugins.otp_static.models import StaticDevice, StaticToken
from django_otp import user_has_device
from django.conf import settings


def totp(request):
    """
    sets context['user_has_device'] True or False if the user is logged in
    """
    if request.user.is_authenticated:
        return {"user_has_device": user_has_device(request.user, confirmed=True)}
    else:
        return {}


def otp_static(request):
    """
    sets context['user_has_tokens'] True or False if the user is logged in
    """
    if request.user.is_authenticated:
        if user_has_device(request.user, confirmed=True):
            try:
                device = StaticDevice.objects.get(user_id=request.user.id)
            except StaticDevice.DoesNotExist:
                return {"user_has_tokens": False}
            except StaticDevice.MultipleObjectsReturned:
                device = device[0]
            try:
                tokens = StaticToken.objects.filter(device_id=device.id)
                return {"user_has_tokens": bool(tokens)}
            except StaticToken.DoesNotExist:
                return {"user_has_tokens": False}
            return {"user_has_tokens": True}
    return {"user_has_tokens": False}


def available_languages(request):
    """
    Puts AVAILABLE_LANGUAGES from settings.py into the request context
    """
    return {"available_languages": settings.AVAILABLE_LANGUAGES}


def is_self_hosted(request):
    return {"is_self_hosted": settings.SELF_HOSTED}
