import os, random, re


class Diceware(object):
    """
    Implements a pseudorandom "Diceware" style passphrases using the
    EFF "long" word list
    """

    def __init__(self, wordlist="wordlists/eff_large_wordlist.txt"):
        path = os.path.dirname(__file__)
        self.wordlist = self.load_list("%s/%s" % (path, wordlist))
        # seed the rng using os.urandom
        self.rng = random.SystemRandom()

    def load_list(self, filename):
        """
        Loads the list from a text file and returns it as a dict
        """
        list = {}
        with open(filename, "r") as f:
            for line in f.readlines():
                m = re.match("([0-9]+)\s+([a-z-]+)$", line)
                if m:
                    list[m.group(1)] = m.group(2)
        return list

    def roll_die(self):
        """
        Returns an integer from 1-6
        """
        return self.rng.randint(1, 6)

    def get_sequence(self, digits=5):
        """
        Returns an n-digit number sequence
        """
        output = 0
        for x in range(0, digits):
            roll = self.roll_die()
            output = output + (roll * 10 ** x)
        return output

    def get_word(self, sequence):
        """
        Returns the word matching the given sequence
        """
        return self.wordlist["%s" % sequence]

    def get_passphrase(self, length=6):
        """
        Returns a space-delimited list of n randomly selected words
        """
        words = []
        for x in range(0, length):
            seq = self.get_sequence()
            words.append(self.get_word(seq))
        return " ".join(words)
