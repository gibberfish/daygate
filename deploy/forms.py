from django import forms
from django.utils.translation import ugettext_lazy
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from django_otp.plugins.otp_totp.models import TOTPDevice
from django_otp.util import hex_validator
from django.core.validators import EmailValidator, RegexValidator, URLValidator
from django.conf import settings, locale
from pytz import common_timezones
from .models import SSHKey


class DomainSetUpForm(forms.Form):
    base_domain = forms.CharField(
        label=ugettext_lazy("Base domain name"),
        help_text=ugettext_lazy("The internet domain name for your Gibberfish server."),
        widget=forms.TextInput(
            attrs={
                "id": "base_domain",
                "placeholder": ugettext_lazy("example:") + " cloud.example.com",
            }
        ),
        validators=[
            RegexValidator(
                regex="[A-Za-z0-9-]+\.[A-Za-z0-9-]+",
                message=ugettext_lazy(
                    "This does not appear to be a valid domain name."
                ),
            )
        ],
    )
    office_domain = forms.CharField(
        label=ugettext_lazy("Collabora domain name"),
        help_text="{} {}".format(
            ugettext_lazy("The sub-domain for the Collabora service."),
            ugettext_lazy("This must resolve to the same IP as the base domain."),
        ),
        widget=forms.TextInput(
            attrs={
                "id": "office_domain",
                "placeholder": ugettext_lazy("example:") + " office.cloud.example.com",
            }
        ),
        validators=[
            RegexValidator(
                regex="[A-Za-z0-9-]+\.[A-Za-z0-9-]+",
                message=ugettext_lazy(
                    "This does not appear to be a valid domain name."
                ),
            )
        ],
    )
    conference_domain = forms.CharField(
        label=ugettext_lazy("Multi-user chat domain name"),
        help_text="{} {}".format(
            ugettext_lazy("The sub-domain for the chat service."),
            ugettext_lazy("This must resolve to the same IP as the base domain."),
        ),
        widget=forms.TextInput(
            attrs={
                "id": "conference_domain",
                "placeholder": ugettext_lazy("example:")
                + " conference.cloud.example.com",
            }
        ),
        validators=[
            RegexValidator(
                regex="[A-Za-z0-9-]+\.[A-Za-z0-9-]+",
                message=ugettext_lazy(
                    "This does not appear to be a valid domain name."
                ),
            )
        ],
    )


class EmailChangeForm(forms.Form):
    email_address = forms.CharField(
        label=ugettext_lazy("Admin email"),
        help_text=ugettext_lazy(
            "This address may receive occasional admin notifications."
        ),
        widget=forms.EmailInput(
            attrs={"id": "email_address", "placeholder": "admin@example.com"}
        ),
        validators=[EmailValidator()],
    )


class SettingsForm(DomainSetUpForm, EmailChangeForm):
    def tz_tuples():
        return [(x, x) for x in common_timezones]

    def available_languages():
        return [
            (
                locale.LANG_INFO[x]["code"],
                "{} ({})".format(
                    locale.LANG_INFO[x]["name_local"], locale.LANG_INFO[x]["code"]
                ),
            )
            for x in settings.AVAILABLE_LANGUAGES
        ]

    TIMEZONES = tz_tuples()
    LANGUAGES = available_languages()

    timezone = forms.ChoiceField(
        label=ugettext_lazy("Server timezone"),
        choices=TIMEZONES,
        help_text=ugettext_lazy(
            "Set the server's timezone for scheduled maintenance tasks."
        ),
    )
    language = forms.ChoiceField(
        label=ugettext_lazy("Default Language"),
        choices=LANGUAGES,
        help_text=ugettext_lazy("This may be overridden by browser settings."),
    )
    pgp_key = forms.CharField(
        required=False,
        # NOTE: Hidden until backend functionality exists
        widget=forms.HiddenInput(),
        #     widget=forms.Textarea(
        #         attrs={
        #             'id': 'pgp_key',
        #             'placeholder': 'Paste your public key here'
        #         }
        #     ),
        label=ugettext_lazy("PGP public key"),
        #     required=False,
        #     help_text=ugettext_lazy('If enabled below, this key will be used to \
        #         encrypt messages sent to your admin email.'),
        #     validators=[
        #         RegexValidator(
        #             regex='^-----BEGIN PGP PUBLIC KEY BLOCK-----',
        #             message=ugettext_lazy(
        #                 'Invalid public key header.'
        #             )
        #         )
        #     ],
        #
    )
    encrypt_emails = forms.BooleanField(
        required=False,
        # NOTE: Hidden until backend functionality exists
        widget=forms.HiddenInput(),
        # widget=forms.CheckboxInput(
        #     attrs={ 'id': 'encrypt_emails'}
        # ),
        # required=False,
        label=ugettext_lazy("Send PGP encrypted emails"),
    )
    show_advanced = forms.BooleanField(
        required=False,
        label=ugettext_lazy("Show advanced options"),
        widget=forms.CheckboxInput(attrs={"id": "show_advanced"}),
    )


class BackupForm(forms.Form):
    CHOICES = [
        ("none", ugettext_lazy("None")),
        ("ssh", ugettext_lazy("SSH")),
        ("cifs", ugettext_lazy("Windows")),
    ]
    method = forms.ChoiceField(
        widget=forms.RadioSelect(),
        choices=CHOICES,
        required=True,
        label=ugettext_lazy("Choose backup method"),
    )
    ssh_target = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "id": "ssh_target",
                "placeholder": ugettext_lazy(
                    "example: user@example.com:/path/to/backup"
                ),
                "autocapitalize": "off",
            }
        ),
        validators=[
            RegexValidator(
                regex="^[A-Za-z0-9_.-]+@[A-Za-z0-9_.-]+:\/",
                message=ugettext_lazy(
                    "Target must follow this format: \
                    user@example.com:/path/to/backup"
                ),
            )
        ],
        label=ugettext_lazy("SSH target"),
        required=False,
    )
    pubkey = forms.CharField(
        widget=forms.Textarea(attrs={"id": "pubkey", "readonly": True}),
        label=ugettext_lazy("Public key"),
        required=False,
    )
    cifs_target = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "id": "cifs_target",
                "placeholder": ugettext_lazy("example: //example.com/share"),
                "autocapitalize": "off",
            }
        ),
        label=ugettext_lazy("Share"),
        validators=[
            RegexValidator(
                regex="^\/\/[A-Za-z0-9_.-]+\/[A-Za-z0-9_.-]+",
                message=ugettext_lazy(
                    "Target must follow this format: //example.com/share"
                ),
            )
        ],
        required=False,
    )
    cifs_username = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "id": "cifs_username",
                "placeholder": ugettext_lazy("Username"),
                "autocapitalize": "off",
            }
        ),
        label=ugettext_lazy("Username"),
        required=False,
    )
    cifs_password = forms.CharField(
        widget=forms.PasswordInput(
            render_value=True,
            attrs={"id": "cifs_password", "placeholder": ugettext_lazy("Password")},
        ),
        label=ugettext_lazy("Password"),
        help_text=str(ugettext_lazy("WARNING:"))
        + " "
        + str(
            ugettext_lazy("This password will be stored in clear text.")
            + " "
            + str(ugettext_lazy("Do not use an account with elevated privileges."))
        ),
        required=False,
    )
    cifs_domain = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "id": "cifs_domain",
                "placeholder": ugettext_lazy("Windows domain"),
                "autocapitalize": "off",
            }
        ),
        label=ugettext_lazy("Windows domain"),
        required=False,
    )
    use_tor = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={"id": "use_tor"}),
        required=False,
        label=ugettext_lazy("Send backups via Tor"),
    )


class DeployForm(forms.Form):
    passphrase = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "id": "passphrase",
                "placeholder": ugettext_lazy("Enter encryption passphrase"),
                "autocomplete": "off",
                "autocapitalize": "off",
            }
        )
    )
    output = forms.CharField(
        widget=forms.Textarea(
            attrs={
                "id": "output",
                # 'rows': 10,
                "readonly": True,
            }
        )
    )


class DNSUpdateForm(forms.Form):
    PROTOCOLS = [("dyndns2", "dyndns2")]

    enabled = forms.BooleanField(label=ugettext_lazy("Enabled"), required=False)
    protocol = forms.ChoiceField(label=ugettext_lazy("Protocol"), choices=PROTOCOLS)
    server = forms.CharField(
        label=ugettext_lazy("Server"),
        widget=forms.TextInput(
            attrs={"placeholder": ugettext_lazy("DNS update server name")}
        ),
        help_text=ugettext_lazy("This server will not be contacted via Tor."),
    )
    domain_name = forms.CharField(
        label=ugettext_lazy("Domain name"),
        widget=forms.TextInput(
            attrs={"placeholder": ugettext_lazy("Fully qualified domain name")}
        ),
    )
    password = forms.CharField(
        label=ugettext_lazy("Password"),
        widget=forms.PasswordInput(
            render_value=True, attrs={"placeholder": ugettext_lazy("Password/Secret")}
        ),
    )


class CustomAuthenticationForm(AuthenticationForm):
    username = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": ugettext_lazy("Username")})
    )
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                # Translators: "password" is an acceptable substitute if there is no term for "passphrase"
                "placeholder": ugettext_lazy("Passphrase")
            }
        )
    )


class TOTPAddDeviceForm(forms.Form):
    confirmation = forms.CharField(
        widget=forms.TextInput(
            attrs={"placeholder": ugettext_lazy("Enter code to confirm"), "size": 30}
        ),
        validators=[hex_validator()],
    )
    key = forms.CharField(widget=forms.HiddenInput())


class StaticTokensForm(forms.Form):
    token_list_csv = forms.CharField(widget=forms.HiddenInput())


class TOTPRemoveDeviceForm(forms.Form):
    pass


class CustomPasswordChangeForm(PasswordChangeForm):
    """
    override the parent form fields to add placeholder text
    """

    old_password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={"placeholder": ugettext_lazy("Old passphrase")}
        )
    )
    new_password1 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={"placeholder": ugettext_lazy("New passphrase")}
        )
    )
    new_password2 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={"placeholder": ugettext_lazy("Confirm new passphrase")}
        )
    )


class AddSSHKeyForm(forms.ModelForm):
    """
    all this for some placeholder text...
    """

    class Meta:
        model = SSHKey
        fields = ["pubkey"]

    pubkey = forms.CharField(
        widget=forms.Textarea(
            attrs={"placeholder": ugettext_lazy("Enter SSH public key")}
        )
    )


class UpdateForm(forms.Form):
    auto_update_daygate = forms.BooleanField(
        required=False,
        label=ugettext_lazy("Update the Management Portal"),
        widget=forms.CheckboxInput(attrs={"id": "auto_update_daygate"}),
    )
    auto_update_pancrypticon = forms.BooleanField(
        required=False,
        label=ugettext_lazy("Update the Cloud Platform"),
        widget=forms.CheckboxInput(attrs={"id": "auto_update_pancrypticon"}),
    )


class PowerControlForm(forms.Form):
    reboot = forms.BooleanField(initial=True, required=False)


class NuclearOptionForm(forms.Form):
    def clean_confirmation_code(self):
        """
        ensure the typed code matches the hidden code
        """
        confirmation_code = self.cleaned_data.get("confirmation_code")
        hidden_confirmation_code = self.cleaned_data.get("hidden_confirmation_code")
        if confirmation_code != hidden_confirmation_code:
            raise forms.ValidationError(ugettext_lazy("The code does not match!"))
        return confirmation_code

    confirm = forms.BooleanField(
        required=True,
        label=ugettext_lazy("Yes, I wish to destroy all data on the server, forever!"),
    )
    hidden_confirmation_code = forms.CharField(widget=forms.HiddenInput())
    confirmation_code = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "id": "passphrase",
                "placeholder": ugettext_lazy("Confirmation code"),
                "autocomplete": "off",
                "autocapitalize": "off",
            }
        )
    )
