from django_otp.plugins.otp_totp.models import TOTPDevice
from django_otp import user_has_device
from django.contrib.auth.models import User
import json, qrcode, re
import qrcode.image.svg
from .models import Config, Profile


def needs_verification(request):
    """
    check if verification is possible but not completed
    """
    if request.user.is_authenticated:
        if (
            user_has_device(request.user, confirmed=True)
            and not request.user.is_verified()
        ):
            return True
    return False


def render_qrcode(user, key):
    """
    draws a QR code as an SVG image
    """
    device = TOTPDevice(user=user, key=key)
    qr = qrcode.QRCode(image_factory=qrcode.image.svg.SvgImage, box_size=20, border=0)
    qr.add_data(device.config_url)
    return qr.make_image(fit=True)


def upsert_row(row, model=None):
    """
    upserts a row from a list of tuples
    """
    for (key, value) in row:
        try:
            setting = model.objects.get(key=key)
            setting.value = value
        except model.DoesNotExist:
            setting = model(key=key, value=value)
        setting.save()


def set_config(key, value):
    """
    upserts a k/v pair to the Config db
    """
    upsert_row([(key, value)], Config)


def get_config(key):
    """
    returns a value for a given key or None
    """
    value = None
    try:
        setting = Config.objects.get(key=key)
        value = setting.value
    except Config.DoesNotExist:
        pass
    return value


def get_user(id):
    """
    retreive a User
    """
    try:
        user = User.objects.get(pk=id)
        return user
    except User.DoesNotExist:
        pass


def set_user(id, field, value):
    """
    Set a User attribute
    """
    try:
        user = User.objects.get(pk=id)
        setattr(user, field, value)
        user.save()
        return True
    except User.DoesNotExist:
        return False


def set_profile(user, field, value):
    """
    Set a Profile attribute
    """
    try:
        profile = Profile.objects.get(user=user)
        setattr(profile, field, value)
        profile.save()
        return True
    except Profile.DoesNotExist:
        return False


def get_profile(user):
    """
    retreive a user Profile
    """
    try:
        profile = Profile.objects.get(user=user)
        return profile
    except Profile.DoesNotExist:
        pass


def get_container_status(id):
    """
    retrieve the status for one container
    """
    status = parse_docker_status(get_config("status_docker"))
    if status is not None:
        for s in status:
            if id == s["id"]:
                return s
    return {}


def json2log(filename):
    """
    convert docker's json logs to plain text
    """
    with open(filename, "rb") as fh:
        log = ""
        # deserialize the log contents
        for raw in fh.readlines():
            line = raw.decode().rstrip()
            if len(line):
                data = json.loads(line)
                log = log + "%s\t%s" % (data["time"], data["log"])
        return log


def parse_docker_status(status_string):
    """
    Deserialize the docker status info from the database
    """
    if status_string is None or len(status_string) == 0:
        return None
    output = []
    for container in status_string.split("|"):
        (id, name, status) = container.split(":", 2)
        ok = False
        if re.match("Up", status) and not re.search("unhealthy", status):
            ok = True
        output.append({"id": id, "name": name, "status": status, "ok": ok})
    return output


def parse_filesystem_status(status_string):
    """
    Deserialize the filesystem status info from the database
    """
    if status_string is None or len(status_string) == 0:
        return None
    output = []
    for filesystem in status_string.split("|"):
        (device, total, used, free, percent_used, mountpoint) = filesystem.split()
        output.append(
            {
                "device": device,
                "total": total,
                "used": used,
                "free": free,
                "percent_used": percent_used,
                "mountpoint": mountpoint,
            }
        )
    return output
