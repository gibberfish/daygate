from django.http import HttpResponseRedirect
from deploy.models import Profile
from deploy.middleware.redirecting_middleware import RedirectingMiddleware
import re


class CreateProfile:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.user.is_authenticated:
            try:
                Profile.objects.get(user=request.user)
            except Profile.DoesNotExist:
                profile = Profile(user=request.user)
                profile.save()
        return self.get_response(request)


class ForcePasswordChange(RedirectingMiddleware):
    def custom_code(self, request):
        if request.user.is_authenticated:
            if request.user.profile.force_password_change:
                if not re.match(
                    "\/accounts\/(password_change|logout)\/?", request.path
                ):
                    return HttpResponseRedirect("/accounts/password_change/")
                else:
                    return True
        return False


class ForceEmailChange(RedirectingMiddleware):
    def custom_code(self, request):
        if request.user.is_authenticated:
            if request.user.profile.force_email_change:
                if not re.match("\/accounts\/(email_change|logout)\/?", request.path):
                    return HttpResponseRedirect("/accounts/email_change/")
                else:
                    return True
        return False
