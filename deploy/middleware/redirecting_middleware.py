import re
from django.http import HttpResponseRedirect


class RedirectingMiddleware:
    """
    Avoids redirect loops by allowing a RedirectingMiddleware to signal
    downstream RedirectingMiddleware to no-op.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        # stay out of django_otp's way
        if re.match("^\/accounts\/totp\/", request.path):
            return response
        # if the skip_middleware attribute is absent or False...
        elif not getattr(response, "skip_middleware", False):
            # run custom_code()...
            result = self.custom_code(request)
            # if it's a boolean value, set response.skip_middleware
            if isinstance(result, bool):
                setattr(response, "skip_middleware", result)
                return response
            # else if it's an HttpResponseRedirect object, return it
            elif isinstance(result, HttpResponseRedirect):
                return result
        # pass through the unmodified response as a fallback
        return response

    def custom_code(self, request):
        """
        Override this method to run your middleware code. It should return
        either an HttpResponseRedirect object, or a boolean to determine the
        value of response.skip_middleware
        """
        return False
