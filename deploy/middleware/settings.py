from django.http import HttpResponseRedirect
from deploy.models import Config
from deploy.middleware.redirecting_middleware import RedirectingMiddleware
import re


class SetUpDomainNames(RedirectingMiddleware):
    def custom_code(self, request):
        if request.user.is_authenticated:
            try:
                Config.objects.get(key="base_domain")
                Config.objects.get(key="office_domain")
                Config.objects.get(key="conference_domain")
            except Config.DoesNotExist:
                if not re.match("\/(domain_setup|accounts\/logout)\/?", request.path):
                    return HttpResponseRedirect("/domain_setup/")
                else:
                    return True
        return False
