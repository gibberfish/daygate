import re
from channels.auth import AuthMiddlewareStack
from django.contrib.auth.models import AnonymousUser
from django.db import close_old_connections
from deploy.functions import get_config


class WebsocketAuthMiddleware:
    def __init__(self, inner):
        # Store the ASGI application we were passed
        self.inner = inner

    def __call__(self, scope):
        """
        validate a token passed via query params - intended to be used by local
        websocket clients which are not authenticated via standard Django auth
        """
        # assume False
        valid_token = False
        # skip unless this is the websocket endpoint
        if re.match(scope["path"], "/ws/"):
            proxied = False
            for (key, value) in scope["headers"]:
                if key == b"x-proxied":
                    proxied = True
                    break
            # only validate connections that have not been proxied via nginx
            if not proxied:
                # fetch the token from the database
                token = get_config("deploy_token")
                if len(scope["query_string"]):
                    (key, value) = scope["query_string"].decode().split("=")
                    # compare the passed token to the one in the db
                    if token == value:
                        valid_token = True
        return self.inner(dict(scope, auth_token=valid_token))


WebsocketAuthMiddlewareStack = lambda inner: WebsocketAuthMiddleware(
    AuthMiddlewareStack(inner)
)
