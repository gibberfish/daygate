# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from sshpubkeys import SSHKey as ssh


class Config(models.Model):
    key = models.CharField("key", max_length=128, primary_key=True)
    value = models.CharField("value", max_length=128, null=True)


class Language(models.Model):
    code = models.CharField("code", max_length=2, db_index=True, primary_key=True)
    name = models.CharField("name", max_length=128)

    def __unicode__(self):
        return "{0}".format(self.name)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    force_password_change = models.BooleanField(default=True)
    force_email_change = models.BooleanField(default=True)
    pgp_key = models.TextField(max_length=4096, null=True)
    encrypt_emails = models.BooleanField(default=False)


class SSHKey(models.Model):
    fingerprint = models.CharField("Fingerprint", max_length=48, primary_key=True)
    pubkey = models.TextField("Public key", max_length=1024)
    name = models.CharField("Name", max_length=128)
    created = models.DateTimeField("Created", auto_now=True)
    added_by = models.ForeignKey(
        User, verbose_name="Added by", on_delete=models.CASCADE
    )
    enabled = models.BooleanField("Enabled", default=True)

    def save(self, *args, **kwargs):
        key = ssh(self.pubkey)
        try:
            key.parse()
        except Exception as err:
            raise ValidationError("Invalid key:", err)
        self.name = self.pubkey.split(" ")[-1]
        self.fingerprint = key.hash_md5()
        super(SSHKey, self).save(*args, **kwargs)
