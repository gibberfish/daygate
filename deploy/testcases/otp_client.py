from django.contrib.auth.models import User
from django.contrib.messages.storage import default_storage
from django.core.handlers.wsgi import WSGIRequest
from django.test import Client
from os import urandom


class FakeMessageStorage(object):
    """
    provide a dummy object with a .add() method
    """

    def add(*args, **kwargs):
        return True


class OTPClient(Client):
    """
    Just like a regular Client, but with 100% more OTP
    """

    def request(self, **request):
        """
        Create a dummy user and set 'request.user.otp_device = True' to fake
        the otp_required() decorator into thinking the user has been verified
        """
        request = WSGIRequest(self._base_environ(**request))
        user = User.objects.create_user(username=str(urandom(16)))
        setattr(user, "otp_device", True)
        setattr(request, "user", user)
        setattr(request, "_messages", FakeMessageStorage())
        return request
