from django.test import TestCase
from django.conf import settings
from django.contrib.auth.models import User
from django_otp.plugins.otp_static.models import StaticDevice
from deploy.context_processors import *


class FakeUser:
    def __init__(self, is_authenticated):
        self.id = 1
        self.is_authenticated = is_authenticated
        self.is_anonymous = True


class FakeRequest:
    def __init__(self, user):
        self.user = user


class ContextProcessorTestCase(TestCase):
    def test_totp(self):
        user = FakeUser(is_authenticated=False)
        self.assertEqual({}, totp(FakeRequest(user)))
        user = FakeUser(is_authenticated=True)
        self.assertTrue("user_has_device" in totp(FakeRequest(user)))

    def test_otp_static(self):
        user = User()
        user.save()
        # no tokens :(
        self.assertEqual({"user_has_tokens": False}, otp_static(FakeRequest(user)))
        device = StaticDevice(user=user)
        device.save()
        token = StaticToken(device=device, token=StaticToken.random_token())
        token.save()
        # 1 token :)
        self.assertEqual({"user_has_tokens": True}, otp_static(FakeRequest(user)))
        token2 = StaticToken(device=device, token=StaticToken.random_token())
        token2.save()
        # 2+ tokens :)
        self.assertEqual({"user_has_tokens": True}, otp_static(FakeRequest(user)))

    def test_available_languages(self):
        self.assertEqual(
            available_languages(FakeRequest(User()))["available_languages"],
            settings.AVAILABLE_LANGUAGES,
        )

    def test_is_self_hosted(self):
        self.assertEqual(
            is_self_hosted(FakeRequest(User()))["is_self_hosted"], settings.SELF_HOSTED
        )
