from django.test import TestCase
from deploy.diceware import *


class DicewareTestCase(TestCase):
    def setUp(self):
        self.dw = Diceware()

    def test_init(self):
        self.assertGreater(len(self.dw.wordlist), 0)
        self.assertEqual(self.dw.wordlist["11142"], "absolute")

    def test_load_list(self):
        list = self.dw.load_list("deploy/wordlists/eff_large_wordlist.txt")
        self.assertGreater(len(list), 0)
        self.assertEqual(list["11142"], "absolute")

    def test_roll_die(self):
        result = self.dw.roll_die()
        self.assertTrue(isinstance(result, int))
        self.assertGreater(result, 0)
        self.assertLess(result, 7)

    def test_get_sequence(self):
        seq = self.dw.get_sequence(digits=5)
        self.assertRegex(str(seq), "^[0-9]{5}")

    def test_get_word(self):
        word = self.dw.get_word("11142")
        self.assertEqual(word, "absolute")

    def test_get_passphrase(self):
        p = self.dw.get_passphrase(length=6)
        self.assertRegex(p, "^[a-z]+ [a-z]+ [a-z]+ [a-z]+ [a-z]+ [a-z]+$")
