from django.test import TestCase
from deploy.forms import *
from django.contrib.auth.models import User


class DomainSetUpFormTestCase(TestCase):
    def test_form_validators(self):
        form = DomainSetUpForm(
            data={
                "base_domain": "cloud.example.com",
                "office_domain": "office.cloud.example.com",
                "conference_domain": "conference.cloud.example.com",
            }
        )
        self.assertTrue(form.is_valid())
        form = DomainSetUpForm(
            data={
                "base_domain": "invalid domain",
                "office_domain": "office.cloud.example.com",
                "conference_domain": "conference.cloud.example.com",
            }
        )
        self.assertFalse(form.is_valid())
        form = DomainSetUpForm(
            data={
                "base_domain": "cloud.example.com",
                "office_domain": "invalid domain",
                "conference_domain": "conference.cloud.example.com",
            }
        )
        self.assertFalse(form.is_valid())
        form = DomainSetUpForm(
            data={
                "base_domain": "cloud.example.com",
                "office_domain": "office.cloud.example.com",
                "conference_domain": "invalid domain",
            }
        )
        self.assertFalse(form.is_valid())


class EmailChangeFormTestCase(TestCase):
    def test_form_validators(self):
        form = EmailChangeForm(data={"email_address": "test@example.com"})
        self.assertTrue(form.is_valid())
        form = EmailChangeForm(data={"email_address": "invalidformat@derp"})
        self.assertFalse(form.is_valid())


class BackupFormTestCase(TestCase):
    def test_methods(self):
        form = BackupForm()
        self.assertTrue(("ssh", "SSH") in form.fields["method"].choices)

    def test_form_validators(self):
        form = BackupForm(
            data={"method": "ssh", "ssh_target": "test@example.com:/backup"}
        )
        self.assertTrue(form.is_valid())
        form = BackupForm(
            data={"method": "ssh", "ssh_target": "test@example.com/backup"}
        )
        self.assertFalse(form.is_valid())

        form = BackupForm(
            data={"method": "cifs", "cifs_target": "//example.com/backup"}
        )
        self.assertTrue(form.is_valid())
        form = BackupForm(
            data={"method": "cifs", "ssh_target": "test@example.com/backup"}
        )
        self.assertFalse(form.is_valid())


class SettingsFormTestCase(TestCase):
    def test_available_languages(self):
        form = SettingsForm()
        self.assertTrue(("en", "English (en)") in form.fields["language"].choices)

    def test_tz_tuples(self):
        form = SettingsForm()
        self.assertTrue(("UTC", "UTC") in form.fields["timezone"].choices)


class DeployFormTestCase(TestCase):
    pass


class DNSUpdateFormTestCase(TestCase):
    def test_protocols(self):
        form = DNSUpdateForm()
        self.assertTrue(("dyndns2", "dyndns2") in form.fields["protocol"].choices)


class CustomAuthenticationFormTestCase(TestCase):
    pass


class TOTPAddDeviceFormTestCase(TestCase):
    def test_form_validators(self):
        form = TOTPAddDeviceForm(data={"confirmation": "deadbeef", "key": "test"})
        self.assertTrue(form.is_valid())
        form = TOTPAddDeviceForm(
            data={"confirmation": "this is not hex", "key": "test"}
        )
        self.assertFalse(form.is_valid())


class StaticTokensFormTestCase(TestCase):
    pass


class TOTPRemoveDeviceFormTestCase(TestCase):
    pass


class CustomPasswordChangeFormTestCase(TestCase):
    pass


class AddSSHKeyFormTestCase(TestCase):
    pass


class UpdateFormTestCase(TestCase):
    pass


class PowerControlFormTestCase(TestCase):
    pass


class NuclearOptionFormTestCase(TestCase):
    def test_clean_confirmation_code(self):
        form = NuclearOptionForm(
            data={
                "confirm": True,
                "confirmation_code": "test",
                "hidden_confirmation_code": "test",
            }
        )
        self.assertTrue(form.is_valid())
        form = NuclearOptionForm(
            data={
                "confirm": True,
                "confirmation_code": "test",
                "hidden_confirmation_code": "mismatch",
            }
        )
        self.assertFalse(form.is_valid())
