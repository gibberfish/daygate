import os
from django.test import Client, RequestFactory, TestCase
from django.http.request import HttpRequest
from django.contrib.auth.models import User
from deploy.functions import *
from deploy.models import Config, Profile


class FunctionsTestCase(TestCase):
    def test_needs_verification(self):
        factory = RequestFactory()
        request = factory.get("/")
        user = User.objects.create_user(username="test", password="test")
        request.user = user
        self.assertFalse(needs_verification(request))

    def test_render_qrcode(self):
        user = User.objects.create_user(username="test", password="test")
        svg = render_qrcode(user=user, key="123456")
        self.assertEqual(str(type(svg)), "<class 'qrcode.image.svg.SvgImage'>")

    def test_upsert_row(self):
        row1 = [("test", "test1")]
        row2 = [("test", "test2")]

        upsert_row(row1, Config)
        setting = Config.objects.get(key="test")
        self.assertEqual(setting.value, "test1")
        upsert_row(row2, Config)
        setting = Config.objects.get(key="test")
        self.assertEqual(setting.value, "test2")

    def test_set_config(self):
        set_config("testconf", "testvalue")
        setting = Config.objects.get(key="testconf")
        self.assertEqual(setting.value, "testvalue")

    def test_get_config(self):
        set_config("testconf", "testvalue")
        self.assertEqual(get_config("testconf"), "testvalue")

    def test_set_profile(self):
        user1 = User.objects.create_user(username="test1", password="test")
        profile = Profile(user=user1)
        profile.save()
        self.assertTrue(set_profile(user1, "force_password_change", False))
        user2 = User.objects.create_user(username="test2", password="test")
        self.assertFalse(set_profile(user2, "force_password_change", False))

    def test_get_profile(self):
        user1 = User.objects.create_user(username="test1", password="test")
        profile = Profile(user=user1)
        profile.save()
        self.assertTrue(isinstance(get_profile(user1), Profile))
        user2 = User.objects.create_user(username="test2", password="test")
        self.assertFalse(isinstance(get_profile(user2), Profile))

    def test_parse_docker_status(self):
        self.assertEqual(parse_docker_status(None), None)
        result = parse_docker_status(
            "000000000000:foo:Up (healthy)|\
            111111111111:bar:Up (unhealthy)|222222222222:baz: Down"
        )
        self.assertTrue(isinstance(result, list))
        self.assertEqual(len(result), 3)
        self.assertTrue(isinstance(result[0], dict))
        self.assertEqual(result[0]["name"], "foo")
        self.assertEqual(result[0]["status"], "Up (healthy)")
        self.assertTrue(result[0]["ok"])
        self.assertFalse(result[1]["ok"])
        self.assertFalse(result[2]["ok"])

    def test_parse_filesystem_status(self):
        self.assertEqual(parse_filesystem_status(None), None)
        result = parse_filesystem_status(
            "device total used free precent_free mountpoint|\
            device total used free precent_free mountpoint"
        )
        self.assertTrue(isinstance(result, list))
        self.assertEqual(len(result), 2)
        self.assertTrue(isinstance(result[0], dict))
        self.assertEqual(len(result[0]), 6)
        self.assertEqual(result[0]["device"], "device")
        self.assertEqual(result[1]["mountpoint"], "mountpoint")

    def test_json2log(self):
        filename = "/tmp/test.json.txt"
        with open(filename, "wb") as file:
            file.write(
                b'{"log":"Starting periodic command scheduler: cron.\\n",'
                + b'"stream":"stdout","time":"2019-04-22T08:01:44.604200795Z"}'
            )
        self.assertEquals(
            json2log(filename),
            "2019-04-22T08:01:44.604200795Z\tStarting periodic command "
            + "scheduler: cron.\n",
        )
        os.remove(filename)

    def test_get_container_status(self):
        # no record exists
        status = get_container_status("000000000000")
        self.assertEqual(status, {})
        # record matches
        row = [("status_docker", "000000000000:foo:Up (healthy)")]
        upsert_row(row, Config)
        status = get_container_status("000000000000")
        self.assertEqual(status["name"], "foo")
        # bad id
        status = get_container_status("12345")
        self.assertEqual(status, {})
