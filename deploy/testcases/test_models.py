from django.test import TestCase
from django.contrib.auth.models import User
from deploy.models import *


class ConfigModelTestCase(TestCase):
    def setUp(self):
        Config.objects.create(key="answer", value=42)

    def test_config_model(self):
        setting = Config.objects.get(key="answer")
        self.assertEqual(setting.value, "42")


class LanguageModelTestCase(TestCase):
    def setUp(self):
        Language.objects.create(code="en", name="English")

    def test_language_model(self):
        language = Language.objects.get(code="en")
        self.assertEqual(language.name, "English")
        self.assertEqual(language.__unicode__(), "English")


class ProfileModelTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="Test Ferguson")
        Profile.objects.create(user_id=self.user.id)

    def test_profile_model(self):
        profile = Profile.objects.get(user_id=self.user.id)
        self.assertEqual(profile.user.username, "Test Ferguson")
        self.assertTrue(profile.force_password_change)
        self.assertTrue(profile.force_email_change)
        self.assertEquals(profile.pgp_key, None)
        self.assertFalse(profile.encrypt_emails)


class SSHKeyModelTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="Test Ferguson")
        SSHKey.objects.create(
            pubkey="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCvYEcr2ec67G56rLXeG"
            + "WSaXkm1dHR8n7SEhY6Icn/zw1LeJjvfYd8d+Qq8gQQ7879I74FCo2dG91n/JpcHW"
            + "y8Ms+ZmY/0o+s7kGvv9+eZrjvqiUJC3UwajKV/8GjpWfqec7ZoZWEntHoWDtid0C"
            + "7GIwOJw2Xr4qyVgGrIJoU/fbNY0djrfiPpQ6Zz8N4qo0HxiIx4zVYKXm+nAA5QmB"
            + "ZTn4ZIoTDiy4CX1x7J48ObZInWXXjmiMuSnpeEaHaj2EugoNXm/3W4aTAYlriEPc"
            + "UeCaJ83IQ+rzbV5YwgRKHVczbDx8YWQ7wyIdB6c7mkZcDkz/AzH5LHePEqviUQR+"
            + "539 test@example.com",
            added_by=self.user,
        )

    def test_sshkey_model(self):
        # the .save() method should automatically compute the fingerprint and
        # name of the submitted key
        sshkey = SSHKey.objects.get(
            fingerprint="MD5:f3:5f:c2:a6:d4:09:0a:6f:af:20:54:ad:ab:c4:46:89"
        )
        self.assertEqual(sshkey.name, "test@example.com")
        self.assertEqual(sshkey.added_by.username, "Test Ferguson")
        self.assertTrue(sshkey.enabled)
