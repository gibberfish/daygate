import binascii, pyotp, re
from ast import literal_eval
from django.conf import settings
from django.test import Client, RequestFactory, SimpleTestCase, TestCase
from django.contrib.auth.models import User
from django.http import Http404
from django_otp.plugins.otp_static.models import StaticDevice
from django_otp.plugins.otp_totp.models import TOTPDevice
from base64 import b32encode
from deploy.views import *
from deploy.models import Config, Profile, SSHKey
from deploy.testcases.otp_client import OTPClient
from deploy.functions import set_profile

c = Client()
otp_c = OTPClient()


def create_user(username="test", password="test"):
    """
    returns a basic User instance + Profile
    """
    user = User.objects.create_user(username=username, password=password)
    Profile.objects.create(
        user=user, force_password_change=False, force_email_change=False
    )
    return user


def set_domains(
    base_domain="example.com",
    office_domain="office.example.com",
    conference_domain="conference.example.com",
):
    Config.objects.create(key="base_domain", value=base_domain)
    Config.objects.create(key="office_domain", value=office_domain)
    Config.objects.create(key="conference_domain", value=conference_domain)


def get_anonymous(self, url, expected):
    """
    makes an anonymous GET and expects to be redirected
    """
    response = c.get(url)
    self.assertRedirects(
        response,
        expected,
        status_code=302,
        target_status_code=200,
        fetch_redirect_response=True,
    )


def get_authenticated(self, url, expected):
    """
    makes an authenticated request and expects to be redirected
    """
    c.force_login(self.user)
    response = c.get(url)
    self.assertRedirects(
        response, expected, target_status_code=200, fetch_redirect_response=True
    )


def test_logout_anonymous(self):
    response = c.get("/accounts/logout/")
    self.assertRedirects(
        response,
        "/accounts/login/?next=/accounts/logout/",
        target_status_code=200,
        fetch_redirect_response=True,
    )


class CustomTestCase(TestCase):
    def setUp(self):
        self.user = create_user()
        set_domains()

    def get_unverified(self, url, pk=None):
        get_anonymous(self, url, "/accounts/login/?next={}".format(url))
        get_authenticated(self, url, "/accounts/login/?next={}".format(url))

    def get_verified(self, view, url, pk=None):
        response = view.as_view()(otp_c.get(url), pk=pk)
        self.assertEquals(response.status_code, 200)


class AddSSHKeyFormViewTestCase(CustomTestCase):
    def test_otp_required(self):
        self.get_unverified("/ssh_keys/add/")
        self.get_verified(AddSSHKeyFormView, "/ssh_keys/add/")

    def test_add_key(self):
        postdata = {
            "pubkey": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCvYEcr2ec67G56r"
            + "LXeGWSaXkm1dHR8n7SEhY6Icn/zw1LeJjvfYd8d+Qq8gQQ7879I74FCo2dG91n/J"
            + "pcHWy8Ms+ZmY/0o+s7kGvv9+eZrjvqiUJC3UwajKV/8GjpWfqec7ZoZWEntHoWDt"
            + "id0C7GIwOJw2Xr4qyVgGrIJoU/fbNY0djrfiPpQ6Zz8N4qo0HxiIx4zVYKXm+nAA"
            + "5QmBZTn4ZIoTDiy4CX1x7J48ObZInWXXjmiMuSnpeEaHaj2EugoNXm/3W4aTAYlr"
            + "iEPcUeCaJ83IQ+rzbV5YwgRKHVczbDx8YWQ7wyIdB6c7mkZcDkz/AzH5LHePEqvi"
            + "UQR+539 test@example.com"
        }
        response = AddSSHKeyFormView.as_view()(
            otp_c.post("/ssh_keys/add/", data=postdata)
        )
        self.assertEquals(response.status_code, 302)
        self.assertEquals(response.url, "/ssh_keys/")
        ssh_key = SSHKey.objects.get(
            fingerprint="MD5:f3:5f:c2:a6:d4:09:0a:6f:af:20:54:ad:ab:c4:46:89"
        )
        self.assertEquals(ssh_key.name, "test@example.com")


class DeleteSSHKeyFormViewTestCase(CustomTestCase):
    def test_otp_required(self):
        self.get_unverified("/ssh_keys/delete/")

    def test_delete_key(self):
        # create a new SSHKey
        ssh_key = SSHKey.objects.create(
            pubkey="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCvYEcr2ec67G56rLXeG"
            + "WSaXkm1dHR8n7SEhY6Icn/zw1LeJjvfYd8d+Qq8gQQ7879I74FCo2dG91n/JpcHW"
            + "y8Ms+ZmY/0o+s7kGvv9+eZrjvqiUJC3UwajKV/8GjpWfqec7ZoZWEntHoWDtid0C"
            + "7GIwOJw2Xr4qyVgGrIJoU/fbNY0djrfiPpQ6Zz8N4qo0HxiIx4zVYKXm+nAA5QmB"
            + "ZTn4ZIoTDiy4CX1x7J48ObZInWXXjmiMuSnpeEaHaj2EugoNXm/3W4aTAYlriEPc"
            + "UeCaJ83IQ+rzbV5YwgRKHVczbDx8YWQ7wyIdB6c7mkZcDkz/AzH5LHePEqviUQR+"
            + "539 test@example.com",
            added_by=self.user,
        )
        # delete it via the form
        response = DeleteSSHKeyFormView.as_view()(
            otp_c.post("/ssh_keys/delete/"),
            pk="MD5:f3:5f:c2:a6:d4:09:0a:6f:af:20:54:ad:ab:c4:46:89",
        )
        self.assertEquals(response.status_code, 302)
        self.assertEquals(response.url, "/ssh_keys/")
        # make sure it's gone from the db
        with self.assertRaises(SSHKey.DoesNotExist):
            SSHKey.objects.get(
                fingerprint="MD5:f3:5f:c2:a6:d4:09:0a:6f:af:20:54:ad:ab:c4:46:89"
            )


class BackupFormViewTestCase(CustomTestCase):
    def test_otp_required(self):
        self.get_unverified("/backup/")
        self.get_verified(BackupFormView, "/backup/")

    def test_get_initial(self):
        c.force_login(self.user)
        set_config("backup_cifs_target", "//example/backup/")
        set_config("backup_cifs_username", "example")
        set_config("backup_cifs_password", "secretpassword")
        set_config("backup_cifs_domain", "EXAMPLE")
        set_config("backup_method", "cifs")
        set_config("backup_ssh_target", "user@example.com:/backup/")
        set_config("backup_use_tor", True)
        response = BackupFormView.as_view()(otp_c.get("/backup/"))
        self.assertEqual(response.status_code, 200)
        initial = response.context_data["form"].initial
        self.assertRegex(initial["pubkey"], "^(ssh-rsa|No key file found)")
        self.assertEqual(initial["cifs_target"], "//example/backup/")
        self.assertEqual(initial["cifs_username"], "example")
        self.assertEqual(initial["cifs_password"], "secretpassword")
        self.assertEqual(initial["cifs_domain"], "EXAMPLE")
        self.assertEqual(initial["method"], "cifs")
        self.assertEqual(initial["ssh_target"], "user@example.com:/backup/")
        self.assertTrue(initial["use_tor"])

    def test_get_unverified(self):
        get_anonymous(self, "/backup/", "/accounts/login/?next=/backup/")
        get_authenticated(self, "/backup/", "/accounts/login/?next=/backup/")

    def test_get_verified(self):
        response = BackupFormView.as_view()(otp_c.get("/backup/"))
        self.assertEquals(response.status_code, 200)

    def test_set_backup_none(self):
        c.force_login(self.user)
        response = BackupFormView.as_view()(
            otp_c.post("/backup/", data={"method": "none"})
        )
        self.assertEquals(response.status_code, 200)
        try:
            # query the config and make sure the method matches
            setting = Config.objects.get(key="backup_method")
            self.assertEqual(setting.value, "none")
            setting = Config.objects.get(key="backup_use_tor")
            self.assertFalse(literal_eval(setting.value))
        except Config.DoesNotExist:
            self.assertTrue(False)

    def test_set_backup_ssh(self):
        c.force_login(self.user)
        response = BackupFormView.as_view()(
            otp_c.post(
                "/backup/",
                data={"method": "ssh", "ssh_target": "foo@example.com:/backups"},
            )
        )
        self.assertEquals(response.status_code, 200)
        try:
            # query the config and make sure the method matches
            setting = Config.objects.get(key="backup_method")
            self.assertEqual(setting.value, "ssh")
            setting = Config.objects.get(key="backup_ssh_target")
            self.assertEqual(setting.value, "foo@example.com:/backups")
            setting = Config.objects.get(key="backup_use_tor")
            self.assertFalse(literal_eval(setting.value))
        except Config.DoesNotExist:
            self.assertTrue(False)

    def test_set_backup_cifs(self):
        c.force_login(self.user)
        response = BackupFormView.as_view()(
            otp_c.post(
                "/backup/",
                data={
                    "method": "cifs",
                    "cifs_target": "//example.com/backups",
                    "cifs_username": "test",
                    "cifs_password": "test",
                    "cifs_domain": "TEST",
                    "use_tor": True,
                },
            )
        )
        self.assertEqual(response.status_code, 200)
        try:
            # query the config and make sure the method matches
            setting = Config.objects.get(key="backup_method")
            self.assertEqual(setting.value, "cifs")
            setting = Config.objects.get(key="backup_cifs_target")
            self.assertEqual(setting.value, "//example.com/backups")
            setting = Config.objects.get(key="backup_cifs_username")
            self.assertEqual(setting.value, "test")
            setting = Config.objects.get(key="backup_cifs_password")
            self.assertEqual(setting.value, "test")
            setting = Config.objects.get(key="backup_cifs_domain")
            self.assertEqual(setting.value, "TEST")
            setting = Config.objects.get(key="backup_use_tor")
            self.assertTrue(literal_eval(setting.value))
        except Config.DoesNotExist:
            self.assertTrue(False)


class DeployFormViewTestCase(CustomTestCase):
    def test_get_unverified(self):
        get_anonymous(self, "/deploy/", "/accounts/login/?next=/deploy/")

    def test_get_authenticated(self):
        c.force_login(self.user)
        response = c.get("/deploy/")
        self.assertEqual(response.status_code, 200)

    def test_deploy(self):
        c.force_login(self.user)
        response = DeployFormView.as_view()(
            otp_c.post("/deploy/", data={"passphrase": "test"})
        )
        self.assertEquals(response.status_code, 200)


class DNSUpdateFormViewTestCase(CustomTestCase):
    def test_get_initial_default(self):
        c.force_login(self.user)
        response = c.get("/nsupdate/")
        initial = response.context_data["form"].initial
        self.assertEqual(response.status_code, 200)
        self.assertEqual(initial["protocol"], settings.NSUPDATE_DEFAULT_PROTOCOL)
        self.assertEqual(initial["server"], settings.NSUPDATE_DEFAULT_SERVER)

    def test_get_initial_custom(self):
        c.force_login(self.user)
        set_config("nsupdate_enabled", True)
        set_config("nsupdate_protocol", "fictionalprotocol")
        set_config("nsupdate_server", "example.com")
        set_config("nsupdate_domain_name", "example.org")
        set_config("nsupdate_password", "secretpassword")
        response = c.get("/nsupdate/")
        initial = response.context_data["form"].initial
        self.assertEqual(response.status_code, 200)
        self.assertTrue(initial["enabled"])
        self.assertEqual(initial["protocol"], "fictionalprotocol")
        self.assertEqual(initial["server"], "example.com")
        self.assertEqual(initial["domain_name"], "example.org")
        self.assertEqual(initial["password"], "secretpassword")

    def test_get_unverified(self):
        get_anonymous(self, "/nsupdate/", "/accounts/login/?next=/nsupdate/")

    def test_get_authenticated(self):
        c.force_login(self.user)
        response = c.get("/nsupdate/")
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        c.force_login(self.user)
        response = DNSUpdateFormView.as_view()(
            otp_c.post(
                "/nsupdate/",
                data={
                    "enabled": True,
                    "protocol": "dyndns2",
                    "server": "test.example.com",
                    "domain_name": "dyn.example.com",
                    "password": "test",
                },
            )
        )
        self.assertEquals(response.status_code, 200)
        try:
            # query the config and make sure the method matches
            setting = Config.objects.get(key="nsupdate_enabled")
            self.assertTrue(literal_eval(setting.value))
            setting = Config.objects.get(key="nsupdate_protocol")
            self.assertEqual(setting.value, "dyndns2")
            setting = Config.objects.get(key="nsupdate_server")
            self.assertEqual(setting.value, "test.example.com")
            setting = Config.objects.get(key="nsupdate_domain_name")
            self.assertEqual(setting.value, "dyn.example.com")
            setting = Config.objects.get(key="nsupdate_password")
            self.assertEqual(setting.value, "test")
        except Config.DoesNotExist:
            self.assertTrue(False)


class DomainSetUpViewTestCase(CustomTestCase):
    def test_get_initial(self):
        c.force_login(self.user)
        response = c.get("/domain_setup/")
        initial = response.context_data["form"].initial
        self.assertEqual(response.status_code, 200)
        self.assertEqual(initial["base_domain"], "example.com")
        self.assertEqual(initial["office_domain"], "office.example.com")
        self.assertEqual(initial["conference_domain"], "conference.example.com")

    def test_post(self):
        c.force_login(self.user)
        response = c.post(
            "/domain_setup/",
            data={
                "base_domain": "cloud.example.com",
                "office_domain": "office.cloud.example.com",
                "conference_domain": "conference.cloud.example.com",
            },
        )
        self.assertRedirects(
            response,
            "/",
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True,
        )
        self.assertEqual(get_config("base_domain"), "cloud.example.com")
        self.assertEqual(get_config("office_domain"), "office.cloud.example.com")
        self.assertEqual(
            get_config("conference_domain"), "conference.cloud.example.com"
        )


class EmailChangeViewTestCase(CustomTestCase):
    def test_get_anonymous(self):
        get_anonymous(
            self,
            "/accounts/email_change/",
            "/accounts/login/?next=/accounts/email_change/",
        )

    def test_get_authenticated(self):
        c.force_login(self.user)
        response = c.get("/accounts/email_change/")
        self.assertEqual(response.status_code, 200)

    def test_change_email(self):
        c.force_login(self.user)
        response = c.post(
            "/accounts/email_change/", data={"email_address": "test@example.com"}
        )
        self.assertRedirects(
            response,
            "/",
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True,
        )
        try:
            # query the user and make sure the email address matches
            user = User.objects.get(pk=self.user.id)
            profile = Profile.objects.get(user_id=self.user.id)
            self.assertEqual(user.email, "test@example.com")
            self.assertFalse(profile.force_email_change)
        except (User.DoesNotExist, Profile.DoesNotExist):
            self.assertTrue(False)


class ListSSHKeysViewTestCase(CustomTestCase):
    def test_otp_required(self):
        self.get_unverified("/ssh_keys/")
        self.get_verified(BackupFormView, "/ssh_keys/")
        self.get_unverified("/ssh_keys/list/")
        self.get_verified(BackupFormView, "/ssh_keys/list/")


class LoginViewTestCase(CustomTestCase):
    def test_get(self):
        response = c.get("/accounts/login/")
        self.assertEqual(response.status_code, 200)

    def test_good_login(self):
        self.assertTrue(c.login(username="test", password="test"))

    def test_bad_login(self):
        self.assertFalse(c.login(username="test", password="password123"))


class LogoutViewTestCase(CustomTestCase):
    def test_logout_logged_in(self):
        c.force_login(self.user)
        response = c.get("/accounts/logout/")
        self.assertEqual(response.status_code, 200)

    def test_logout_logged_out(self):
        c.force_login(self.user)
        c.logout()
        response = c.get("/accounts/logout/")
        self.assertRedirects(
            response,
            "/accounts/login/?next=/accounts/logout/",
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True,
        )


class QRCodeViewTestCase(CustomTestCase):
    def test_qrcode(self):
        c.force_login(self.user)
        response = c.get("/totp/qrcode/deadbeef/")
        self.assertEqual(response.status_code, 200)
        with self.assertRaises(binascii.Error):
            c.get("/totp/qrcode/nothexidecimal/")


class VPNClientDownloadTestCase(CustomTestCase):
    def test_otp_required(self):
        self.get_unverified("/vpn/download/")
        self.get_verified(BackupFormView, "/vpn/download/")

    def test_download(self):
        file_path = "/srv/pancrypticon/openvpn/vpnclient.zip"
        if os.path.exists(file_path):
            response = vpn_client_download(otp_c.get("/vpn/download/"))
            self.assertEqual(response.status_code, 200)
            self.assertEqual(
                response._headers["content-type"], ("Content-Type", "application/zip")
            )
            self.assertEqual(
                response._headers["content-disposition"],
                ("Content-Disposition", "inline; filename=vpnclient.zip"),
            )
        else:
            with self.assertRaises(Http404):
                response = vpn_client_download(otp_c.get("/vpn/download/"))


class LogDownloadTestCase(CustomTestCase):
    def test_download(self):
        c.force_login(self.user)
        file_path = "/home/daygate/daygate/webroot/logs/000000000000.json.txt"
        if os.path.exists(file_path):
            response = log_download(
                c.get("/logs/download/"), container_id="000000000000"
            )
            self.assertEqual(response.status_code, 200)
            self.assertEqual(
                response._headers["content-type"], ("Content-Type", "text/plain")
            )
            self.assertEqual(
                response._headers["content-disposition"],
                (
                    "Content-Disposition",
                    "attachment; filename=000000000000-000000000000.log",
                ),
            )
        else:
            with self.assertRaises(Http404):
                response = log_download(
                    c.get("/logs/download/"), container_id="000000000000"
                )


class SettingsFormViewTestCase(CustomTestCase):
    def test_get_initial_default(self):
        c.force_login(self.user)
        response = c.get("/settings/")
        self.assertEqual(response.status_code, 200)
        initial = response.context["form"].initial
        self.assertEqual(initial["timezone"], settings.TIME_ZONE)
        self.assertEqual(initial["language"], settings.LANGUAGE_CODE)
        self.assertEqual(initial["base_domain"], "example.com")
        self.assertEqual(initial["office_domain"], "office.example.com")
        self.assertEqual(initial["conference_domain"], "conference.example.com")

    def test_get_initial_custom(self):
        c.force_login(self.user)
        set_config("timezone", "Test")
        set_config("language", "de")
        self.user.email = "test@example.com"
        self.user.save()
        profile = get_profile(self.user)
        response = c.get("/settings/")
        self.assertEqual(response.status_code, 200)
        initial = response.context["form"].initial
        self.assertEqual(initial["timezone"], "Test")
        self.assertEqual(initial["language"], "de")
        self.assertEqual(initial["email_address"], "test@example.com")
        self.assertEqual(initial["pgp_key"], profile.pgp_key)
        self.assertEqual(initial["encrypt_emails"], profile.encrypt_emails)

    def test_post(self):
        c.force_login(self.user)
        response = c.post(
            "/settings/",
            data={
                "language": "en",
                "email_address": "test@example.com",
                "pgp_key": "fake key",
                "encrypt_emails": False,
                "timezone": "UTC",
                "base_domain": "cloud.example.com",
                "office_domain": "office.cloud.example.com",
                "conference_domain": "conference.cloud.example.com",
            },
        )
        self.assertRedirects(
            response,
            "/settings/",
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True,
        )
        try:
            # query the config and make sure the method matches
            setting = Config.objects.get(key="language")
            self.assertEqual(setting.value, "en")
            setting = Config.objects.get(key="timezone")
            self.assertEqual(setting.value, "UTC")
            setting = Config.objects.get(key="base_domain")
            self.assertEqual(setting.value, "cloud.example.com")
            setting = Config.objects.get(key="office_domain")
            self.assertEqual(setting.value, "office.cloud.example.com")
            setting = Config.objects.get(key="conference_domain")
            self.assertEqual(setting.value, "conference.cloud.example.com")
            self.user.refresh_from_db()
            self.assertEqual(self.user.email, "test@example.com")
            self.assertEqual(self.user.profile.pgp_key, "fake key")
            self.assertFalse(self.user.profile.encrypt_emails)
        except Config.DoesNotExist:
            self.assertTrue(False)


class CustomPasswordChangeViewTestCase(CustomTestCase):
    def test_post(self):
        set_profile(self.user, "force_password_change", True)
        c.force_login(self.user)
        response = c.post(
            "/accounts/password_change/",
            data={
                "old_password": "test",
                "new_password1": "new password",
                "new_password2": "new password",
            },
        )
        self.assertRedirects(
            response,
            "/",
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True,
        )
        response = c.post(
            "/accounts/password_change/",
            data={
                "old_password": "test",
                "new_password1": "new password",
                "new_password2": "new password",
            },
        )
        self.assertEqual(response.status_code, 400)
        self.user.refresh_from_db()
        self.assertFalse(self.user.profile.force_password_change)


class StaticTokensFormViewTestCase(CustomTestCase):
    def setUp(self):
        self.tokens = []
        super(StaticTokensFormViewTestCase, self).setUp()

    def test_otp_required(self):
        # TODO: Fix return code issue
        # self.get_unverified('/totp/static')
        self.get_verified(StaticTokensFormView, "/totp/static/")

    def test_no_tokens(self):
        c.force_login(self.user)
        response = StaticTokensFormView.as_view()(otp_c.get("/totp/static/"))
        self.tokens = response.context_data["tokens"]
        self.assertEqual(response.status_code, 200)
        self.assertTrue(isinstance(response.context_data["tokens"], list))
        self.assertFalse("user_has_token" in response.context_data)

    def test_save_tokens(self):
        c.force_login(self.user)
        response = StaticTokensFormView.as_view()(
            otp_c.post("/totp/static/", data={"token_list_csv": ",".join(self.tokens)})
        )
        self.assertEqual(response.status_code, 200)
        response = StaticTokensFormView.as_view()(otp_c.get("/totp/static/"))
        self.assertFalse("user_has_token" in response.context_data)


# class TOTPAddDeviceFormViewTestCase(TestCase):
#     def setUp(self):
#         self.user = create_user()
#         self.device_key = ''
#         set_domains()
#
#     def test_get(self):
#         c.force_login(self.user)
#         response = c.get('/totp/add/')
#         self.assertTrue('device_key' in response.context_data)
#         self.assertTrue(
#             re.match('[a-z0-9]{40}', response.context_data['device_key'])
#         )
#
#     def test_add_device(self):
#         c.force_login(self.user)
#         device = TOTPDevice()
#         totp = pyotp.TOTP(b32encode(device.key.encode()))
#         print(device.key)
#         print(totp.now())
#         response = c.post(
#             '/totp/add/',
#             data={
#                 'key': device.key,
#                 'confirmation': totp.now()
#             }
#         )
#         # self.assertRedirects(
#         #     response,
#         #     '/',
#         #     status_code=302,
#         #     target_status_code=200,
#         #     fetch_redirect_response=True
#         # )
#         self.assertEqual(response.status_code, 200)
#         try:
#             print(StaticDevice.objects.all())
#             print(TOTPDevice.objects.all())
#             # TOTPDevice.objects.get(key=key)
#         except TOTPDevice.DoesNotExist:
#             self.assertTrue(False)


def TOTPRemoveDeviceFormViewTestCase(CustomTestCase):
    def setUp(self):
        sd = StaticDevice(self.user)
        sd.save()
        super(TOTPRemoveDeviceFormViewTestCase, self).setUp()

    def test_otp_required(self):
        self.get_unverified("/totp/remove/")
        self.get_verified(TOTPRemoveDeviceFormView, "/totp/remove/")

    def test_remove_device(self):
        try:
            static_devices = StaticDevice.objects.filter(user_id=request.user.id)
        except StaticDevice.DoesNotExist:
            self.assertTrue(False)
        c.force_login(self.user)
        response = TOTPRemoveDeviceFormView.as_view()(
            otp_c.post("/totp/remove/", data={})
        )
        self.assertRedirects(
            response,
            "/totp/add/",
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True,
        )
        try:
            static_devices = StaticDevice.objects.filter(user_id=request.user.id)
        except:
            self.assertRaises(StaticDevice.DoesNotExist)
        else:
            self.assertTrue(False)


class UpdateFormViewTestCase(CustomTestCase):
    def test_get_initial(self):
        set_config("auto_update_daygate", False)
        set_config("auto_update_pancrypticon", False)
        c.force_login(self.user)
        response = c.get("/updates/")
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context["form"].initial["auto_update_daygate"])
        self.assertFalse(response.context["form"].initial["auto_update_pancrypticon"])

    def test_update_form(self):
        c.force_login(self.user)
        response = c.post(
            "/updates/",
            data={"auto_update_daygate": True, "auto_update_pancrypticon": True},
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(literal_eval(get_config("auto_update_daygate")))
        self.assertTrue(literal_eval(get_config("auto_update_pancrypticon")))


class VPNClientViewTestCase(CustomTestCase):
    def test_get(self):
        c.force_login(self.user)
        response = VPNClientView.as_view()(otp_c.get("/vpn/client/"))
        if os.path.isfile("/srv/pancrypticon/openvpn/vpnclient.zip"):
            self.assertTrue(response.context_data["file_exists"])
        else:
            self.assertFalse(response.context_data["file_exists"])


class PowerControlFormViewTestCase(CustomTestCase):
    def test_otp_required(self):
        self.get_unverified("/power_control/")
        self.get_verified(PowerControlFormView, "/power_control/")

    def test_reboot(self):
        c.force_login(self.user)
        response = PowerControlFormView.as_view()(
            otp_c.post("/power_control/", data={"reboot": True})
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(literal_eval(get_config("power_control_reboot")))


class NuclearOptionFormViewTestCase(CustomTestCase):
    def test_otp_required(self):
        self.get_unverified("/nuke/")
        self.get_verified(NuclearOptionFormView, "/nuke/")

    def test_get_initial(self):
        c.force_login(self.user)
        r = NuclearOptionFormView.as_view()(otp_c.get("/nuke/"))
        self.assertEqual(r.status_code, 200)
        self.assertGreater(
            len(r.context_data["form"].initial["hidden_confirmation_code"]), 0
        )

    def test_nuke_bad_code(self):
        c.force_login(self.user)
        response = NuclearOptionFormView.as_view()(
            otp_c.post(
                "/nuke/",
                data={
                    "confirm": True,
                    "hidden_confirmation_code": "one two three",
                    "confirmation_code": "not entered correctly",
                },
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertIsNone(get_config("self_destruct"))
        self.assertIsNone(get_config("self_destruct_confirm"))

    def test_nuke_good_code(self):
        c.force_login(self.user)
        response = NuclearOptionFormView.as_view()(
            otp_c.post(
                "/nuke/",
                data={
                    "confirm": True,
                    "hidden_confirmation_code": "one two three",
                    "confirmation_code": "one two three",
                },
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(literal_eval(get_config("self_destruct")))
        self.assertTrue(literal_eval(get_config("self_destruct_confirm")))


class StatusViewTestCase(CustomTestCase):
    def test_get_unverified(self):
        get_anonymous(self, "/", "/accounts/login/?next=/")

    def test_get_authenticated(self):
        c.force_login(self.user)
        response = c.get("/")
        self.assertEqual(response.status_code, 200)
