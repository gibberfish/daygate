from django.urls import path, re_path, include, reverse_lazy
from django.contrib.auth.decorators import login_required
from django_otp.decorators import otp_required
from django.contrib.auth import views as auth_views
from .views import *
from .forms import CustomAuthenticationForm, CustomPasswordChangeForm, EmailChangeForm

urlpatterns = [
    path(r"", login_required(StatusView.as_view()), name="index"),
    path(
        r"accounts/login/",
        auth_views.LoginView.as_view(authentication_form=CustomAuthenticationForm),
        name="login",
    ),
    path(
        r"accounts/logout/",
        login_required(auth_views.LogoutView.as_view()),
        name="logout",
    ),
    path(r"accounts/totp/", login_required(totp), name="totp"),
    path(
        r"accounts/email_change/",
        login_required(
            EmailChangeView.as_view(
                form_class=EmailChangeForm, success_url=reverse_lazy("deploy")
            )
        ),
        name="email_change",
    ),
    path(
        r"accounts/password_change/",
        login_required(
            CustomPasswordChangeView.as_view(
                form_class=CustomPasswordChangeForm, success_url=reverse_lazy("deploy")
            )
        ),
        name="password_change",
    ),
    path(r"backup/", otp_required(BackupFormView.as_view()), name="backup"),
    path(r"deploy/", login_required(DeployFormView.as_view()), name="deploy"),
    path(
        r"domain_setup/",
        login_required(
            DomainSetUpView.as_view(
                form_class=DomainSetUpForm, success_url=reverse_lazy("deploy")
            )
        ),
        name="domain_setup",
    ),
    path(
        r"logs/download/<container_id>/",
        login_required(log_download),
        name="log_download",
    ),
    path(r"nsupdate/", login_required(DNSUpdateFormView.as_view()), name="nsupdate"),
    path(r"nuke/", otp_required(NuclearOptionFormView.as_view()), name="nuke"),
    path(
        r"power_control/",
        otp_required(PowerControlFormView.as_view()),
        name="power_control",
    ),
    path(r"status/", login_required(StatusView.as_view()), name="status"),
    path(
        r"totp/add/",
        login_required(TOTPAddDeviceFormView.as_view()),
        name="totp_add_device",
    ),
    path(r"totp/qrcode/<key>/", login_required(qrcode), name="qrcode"),
    path(
        r"totp/remove/",
        otp_required(TOTPRemoveDeviceFormView.as_view()),
        name="totp_remove_device",
    ),
    path(
        r"totp/static/",
        otp_required(StaticTokensFormView.as_view()),
        name="static_tokens",
    ),
    path(
        r"settings/",
        login_required(SettingsFormView.as_view(form_class=SettingsForm)),
        name="settings",
    ),
    path(
        r"ssh_keys/add/", otp_required(AddSSHKeyFormView.as_view()), name="add_ssh_key"
    ),
    path(
        r"ssh_keys/delete/<pk>/",
        otp_required(DeleteSSHKeyFormView.as_view()),
        name="delete_ssh_key",
    ),
    re_path(
        r"ssh_keys/(list/)?",
        otp_required(ListSSHKeysView.as_view()),
        name="list_ssh_keys",
    ),
    path(r"updates/", login_required(UpdateFormView.as_view()), name="updates"),
    path(r"vpn/client/", otp_required(VPNClientView.as_view()), name="vpn_client"),
    path(
        r"vpn/download/", otp_required(vpn_client_download), name="vpn_client_download"
    ),
]
