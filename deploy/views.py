# -*- coding: utf-8 -*-
import json, os.path, pytz
from ast import literal_eval
from datetime import datetime
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView, DeleteView, FormView
from django.views.generic import TemplateView
from django.http import (
    HttpResponse,
    HttpResponseRedirect,
    HttpResponseBadRequest,
    Http404,
)
from django.urls import reverse, reverse_lazy
from django.utils.translation import ugettext
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import views as auth_views
from django.contrib.auth import update_session_auth_hash
from django_otp import login as django_otp_login
from django_otp.decorators import otp_required
from django_otp.forms import OTPTokenForm
from django_otp.plugins.otp_totp.models import TOTPDevice
from django_otp.plugins.otp_static.models import StaticDevice, StaticToken
from django.shortcuts import render, redirect
from django.utils.translation import ugettext, LANGUAGE_SESSION_KEY
from functools import partial
from .forms import (
    SettingsForm,
    AddSSHKeyForm,
    BackupForm,
    DeployForm,
    DNSUpdateForm,
    DomainSetUpForm,
    EmailChangeForm,
    StaticTokensForm,
    TOTPAddDeviceForm,
    TOTPRemoveDeviceForm,
    CustomPasswordChangeForm,
    UpdateForm,
    PowerControlForm,
    NuclearOptionForm,
)
from .functions import (
    needs_verification,
    render_qrcode,
    get_config,
    set_config,
    get_profile,
    set_profile,
    get_user,
    set_user,
    parse_docker_status,
    parse_filesystem_status,
    get_container_status,
    json2log,
)
from .models import Config, SSHKey
from .diceware import Diceware


def totp(request):
    """
    prompt users to verify with TOTP if they have a device set up
    """
    if needs_verification(request):
        # present them with a challenge
        form_class = partial(OTPTokenForm, request.user)
        return auth_views.LoginView.as_view(
            template_name="deploy/totp.html", authentication_form=form_class
        )(request)
    # TODO Make this redirect to the original destination
    return HttpResponseRedirect("/")


def qrcode(request, key=None):
    """
    render a QR code as an SVG image
    """
    try:
        img = render_qrcode(request.user, key)
        response = HttpResponse(content_type="image/svg+xml")
        img.save(response)
    except ImportError:
        response = HttpResponse("", status=503)
    return response


def vpn_client_download(request):
    file_path = "/srv/pancrypticon/openvpn/vpnclient.zip"
    if os.path.exists(file_path):
        with open(file_path, "rb") as fh:
            response = HttpResponse(fh.read(), content_type="application/zip")
            response["Content-Disposition"] = "inline; filename=" + os.path.basename(
                file_path
            )
            return response
    raise Http404


def log_download(request, container_id):
    file_path = "%s/webroot/logs/%s.json.txt" % (settings.BASE_DIR, container_id)
    if os.path.exists(file_path):
        status = get_container_status(container_id)
        log_name = status["name"] if "name" in status else container_id
        log = json2log(file_path)
        response = HttpResponse(log, content_type="text/plain")
        response["Content-Disposition"] = "attachment; filename=%s-%s.log" % (
            log_name,
            container_id,
        )
        return response
    raise Http404


class GenericFormView(FormView):
    def check_totp(self, **kwargs):
        if needs_verification(self.request):
            return HttpResponseRedirect("/accounts/totp/")
        return super(GenericFormView, self).get(self.request, **kwargs)

    def get(self, request, **kwargs):
        return self.check_totp(**kwargs)

    def post(self, request, **kwargs):
        return self.check_totp(**kwargs)


class GenericTemplateView(TemplateView):
    def check_totp(self, **kwargs):
        if needs_verification(self.request):
            return HttpResponseRedirect("/accounts/totp/")
        return super(GenericTemplateView, self).get(self.request, **kwargs)

    def get(self, request, **kwargs):
        return self.check_totp(**kwargs)


class SettingsFormView(GenericFormView):
    template_name = "deploy/settings.html"
    form_class = SettingsForm

    def get_context_data(self, **kwargs):
        context = super(GenericFormView, self).get_context_data(**kwargs)
        context["page_title"] = ugettext("Settings")
        return context

    def get_initial(self):
        """
        load the initial form values from the db
        """
        initial = {
            "timezone": get_config("timezone") or settings.TIME_ZONE,
            "language": get_config("language") or settings.LANGUAGE_CODE,
            "base_domain": get_config("base_domain"),
            "office_domain": get_config("office_domain"),
            "conference_domain": get_config("conference_domain"),
        }
        user = get_user(self.request.user.id)
        if user is not None:
            initial["email_address"] = user.email
        profile = get_profile(self.request.user)
        if profile is not None:
            initial["pgp_key"] = profile.pgp_key
            initial["encrypt_emails"] = profile.encrypt_emails
        return initial

    def post(self, request, **kwargs):
        form = SettingsForm(request.POST)
        if form.is_valid():
            lang_code = form.cleaned_data.get("language")
            set_user(request.user.id, "email", form.cleaned_data.get("email_address"))
            set_profile(request.user, "pgp_key", form.cleaned_data.get("pgp_key"))
            set_profile(
                request.user, "encrypt_emails", form.cleaned_data.get("encrypt_emails")
            )
            set_config("timezone", form.cleaned_data.get("timezone"))
            set_config("language", lang_code)
            set_config("base_domain", form.cleaned_data.get("base_domain"))
            set_config("office_domain", form.cleaned_data.get("office_domain"))
            set_config("conference_domain", form.cleaned_data.get("conference_domain"))
            if hasattr(request, "session"):
                request.session[LANGUAGE_SESSION_KEY] = lang_code
            messages.success(
                request, ugettext("Your settings were updated successfully!")
            )
        else:
            messages.error(
                request,
                ugettext("Could not save settings. Please check the form for errors."),
            )
        return HttpResponseRedirect(reverse("settings"))


class AddSSHKeyFormView(CreateView):
    model = SSHKey
    form_class = AddSSHKeyForm
    template_name = "deploy/add_ssh_key.html"
    success_url = reverse_lazy("list_ssh_keys")

    def get_context_data(self, **kwargs):
        context = super(CreateView, self).get_context_data(**kwargs)
        context["page_title"] = ugettext("SSH Keys")
        return context

    def form_valid(self, form):
        form.instance.added_by = self.request.user
        return super(AddSSHKeyFormView, self).form_valid(form)


class BackupFormView(GenericFormView):
    form_class = BackupForm
    template_name = "deploy/backup.html"

    def get_context_data(self, **kwargs):
        context = super(GenericFormView, self).get_context_data(**kwargs)
        context["page_title"] = ugettext("Backups")
        return context

    def get_initial(self):
        """
        load the initial form value from the db
        """
        initial = {}
        backup_method = None
        if os.path.isfile(settings.BACKUP_PUBLIC_KEY):
            with open(settings.BACKUP_PUBLIC_KEY, "r") as f:
                initial["pubkey"] = f.readlines()[0].rstrip()
        else:
            initial["pubkey"] = ugettext("No key file found.")
        fields = [
            "cifs_target",
            "cifs_username",
            "cifs_password",
            "cifs_domain",
            "method",
            "ssh_target",
        ]
        for field in fields:
            initial["{}".format(field)] = get_config("backup_{}".format(field))
        use_tor = "{}".format(get_config("backup_use_tor"))
        initial["use_tor"] = literal_eval(use_tor)
        return initial

    def post(self, request, **kwargs):
        form = BackupForm(request.POST)
        if form.is_valid():
            # get backup method
            form_value = form.cleaned_data.get("method")
            set_config("backup_method", form_value)
            # choose which fields to process based on backup_method's value
            fields = []
            if form_value == "ssh":
                fields = ["ssh_target"]
            elif form_value == "cifs":
                fields = [
                    "cifs_target",
                    "cifs_username",
                    "cifs_password",
                    "cifs_domain",
                ]
            for field in fields:
                form_value = form.cleaned_data.get(field)
                set_config("backup_{}".format(field), form_value)
            form_value = form.cleaned_data.get("use_tor")
            set_config("backup_use_tor", form_value)
            messages.success(
                request, ugettext("Your settings were saved successfully!")
            )
        return self.check_totp(**kwargs)


class CustomPasswordChangeView(GenericFormView, auth_views.PasswordChangeView):
    def get_context_data(self, **kwargs):
        context = super(GenericFormView, self).get_context_data(**kwargs)
        context["page_title"] = ugettext("Password Change")
        return context

    def post(self, request, **kwargs):
        form = CustomPasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            set_profile(request.user, "force_password_change", False)
            update_session_auth_hash(request, user)  # Important!
            messages.success(
                request, ugettext("Your password was successfully updated!")
            )
            return redirect("index")
        return HttpResponseBadRequest()


class DeleteSSHKeyFormView(DeleteView):
    model = SSHKey
    success_url = reverse_lazy("list_ssh_keys")

    def get_context_data(self, **kwargs):
        context = super(DeleteView, self).get_context_data(**kwargs)
        context["page_title"] = ugettext("SSH Keys")
        return context


class DeployFormView(GenericFormView):
    template_name = "deploy/deploy.html"
    form_class = DeployForm

    def get_context_data(self, **kwargs):
        dw = Diceware()
        context = super(DeployFormView, self).get_context_data(**kwargs)
        context["admin_email"] = self.request.user.email
        context["admin_passphrase"] = dw.get_passphrase()
        context["language"] = get_config("language") or settings.LANGUAGE_CODE
        context["nextcloud_domain_name"] = (
            get_config("base_domain") or settings.NEXTCLOUD_DOMAIN_NAME
        )
        context["office_domain_name"] = get_config(
            "office_domain"
        ) or "office.{}".format(settings.NEXTCLOUD_DOMAIN_NAME)
        context["conference_domain_name"] = get_config(
            "conference_domain"
        ) or "conference.{}".format(settings.NEXTCLOUD_DOMAIN_NAME)
        context["hidden_domain_name"] = settings.NEXTCLOUD_HIDDEN_DOMAIN_NAME
        context["testing"] = settings.TESTING
        context["git_branch"] = settings.PANCRYPTICON_GIT_BRANCH
        context["first_run"] = get_config("deployed") or True
        context["page_title"] = ugettext("Deploy")
        return context


class DNSUpdateFormView(GenericFormView):
    form_class = DNSUpdateForm
    template_name = "deploy/dns_update.html"

    def get_context_data(self, **kwargs):
        context = super(GenericFormView, self).get_context_data(**kwargs)
        context["page_title"] = ugettext("DNS")
        return context

    def get_initial(self):
        """
        load the initial form values from the db
        """
        return {
            "enabled": get_config("nsupdate_enabled"),
            "protocol": get_config("nsupdate_protocol")
            or settings.NSUPDATE_DEFAULT_PROTOCOL,
            "server": get_config("nsupdate_server") or settings.NSUPDATE_DEFAULT_SERVER,
            "domain_name": get_config("nsupdate_domain_name"),
            "password": get_config("nsupdate_password"),
        }

    def post(self, request, **kwargs):
        form = DNSUpdateForm(request.POST)
        if form.is_valid():
            nsupdate_enabled_value = form.cleaned_data.get("enabled")
            nsupdate_protocol_value = form.cleaned_data.get("protocol")
            nsupdate_server_value = form.cleaned_data.get("server")
            nsupdate_domain_name_value = form.cleaned_data.get("domain_name")
            nsupdate_password_value = form.cleaned_data.get("password")
            set_config("nsupdate_enabled", nsupdate_enabled_value)
            set_config("nsupdate_protocol", nsupdate_protocol_value)
            set_config("nsupdate_server", nsupdate_server_value)
            set_config("nsupdate_domain_name", nsupdate_domain_name_value)
            set_config("nsupdate_password", nsupdate_password_value)
            messages.success(
                request, ugettext("Your settings were saved successfully!")
            )
        return self.check_totp(**kwargs)


class DomainSetUpView(GenericFormView):
    form_class = DomainSetUpForm
    template_name = "deploy/domain_setup.html"

    def get_context_data(self, **kwargs):
        context = super(GenericFormView, self).get_context_data(**kwargs)
        context["page_title"] = ugettext("Settings")
        return context

    def get_initial(self):
        """
        load the initial form values from the db
        """
        return {
            "base_domain": get_config("base_domain"),
            "office_domain": get_config("office_domain"),
            "conference_domain": get_config("conference_domain"),
        }

    def post(self, request, **kwargs):
        form = DomainSetUpForm(request.POST)
        if form.is_valid():
            set_config("base_domain", form.cleaned_data.get("base_domain"))
            set_config("office_domain", form.cleaned_data.get("office_domain"))
            set_config("conference_domain", form.cleaned_data.get("conference_domain"))
        messages.success(
            request, ugettext("Your domains were configured successfully!")
        )
        return redirect("index")


class EmailChangeView(GenericFormView):
    template_name = "deploy/email_change.html"
    form_class = EmailChangeForm

    def get_context_data(self, **kwargs):
        context = super(GenericFormView, self).get_context_data(**kwargs)
        context["page_title"] = ugettext("Settings")
        return context

    def post(self, request, **kwargs):
        form = EmailChangeForm(request.POST)
        if form.is_valid():
            if set_user(
                request.user.id, "email", form.cleaned_data.get("email_address")
            ):
                set_profile(request.user, "force_email_change", False)
                messages.success(
                    request, ugettext("Your email address was updated successfully!")
                )
            else:
                messages.error(request, ugettext("Error updating email address!"))
        return redirect("index")


class ListSSHKeysView(TemplateView):
    template_name = "deploy/list_ssh_keys.html"

    def get_context_data(self, **kwargs):
        try:
            keys = SSHKey.objects.all()
        except SSHKeys.DoesNotExist:
            keys = None
        context = super(ListSSHKeysView, self).get_context_data(**kwargs)
        context["ssh_keys"] = keys
        context["page_title"] = ugettext("SSH Keys")
        return context


class StaticTokensFormView(GenericFormView):
    """
    Generated a list of static tokens and adds them to the user's StaticDevice
    """

    template_name = "deploy/static_tokens.html"
    form_class = StaticTokensForm

    def check_tokens(self, **kwargs):
        """
        Returns True if the user has 1 or more static tokens
        """
        context = self.get_context_data()
        return context.get("user_has_token")

    def get_context_data(self, **kwargs):
        """
        Generate n tokens and stash them in the request context
        """
        context = super(StaticTokensFormView, self).get_context_data(**kwargs)
        tokens = []
        for i in range(5):
            tokens.append(StaticToken.random_token())
        context["tokens"] = tokens
        context["page_title"] = ugettext("Configure 2FA!")
        return context

    def get(self, request, **kwargs):
        # redirect if the user still has tokens left
        if self.check_tokens(**kwargs):
            return HttpResponseRedirect("/")
        return super(StaticTokensFormView, self).get(request, **kwargs)

    def post(self, request):
        context = self.get_context_data()
        form = StaticTokensForm(request.POST)
        if form.is_valid():
            # get the user's StaticDevice
            try:
                device = StaticDevice.objects.get(user_id=request.user.id)
            except StaticDevice.DoesNotExist:
                # this shouldn't be possible, but just in case, handle it
                # without returning a 500 error
                return self.form_invalid(form)
            except StaticDevice.MultipleObjectsReturned:
                # if somehow the user ends up with two StaticDevices, pick the
                # first one
                device = device[0]

            # get the tokens from the request context
            tokens = form.cleaned_data.get("token_list_csv").split(",")
            # feed them into the db
            for token in tokens:
                t = StaticToken(device=device, token=token)
                t.save()
            return HttpResponseRedirect("/")
        else:
            context["form"] = form
            return self.form_invalid(form)


class TOTPAddDeviceFormView(GenericFormView):
    """
    Adds a TOTP device to the user's account
    """

    template_name = "deploy/totp_add_device.html"
    form_class = TOTPAddDeviceForm

    def get_context_data(self, **kwargs):
        # instantiate a temporary Device to generate a key to pass off to the
        # template
        device = TOTPDevice(user=self.request.user)
        context = super(TOTPAddDeviceFormView, self).get_context_data(**kwargs)
        context["device_key"] = device.key
        context["page_title"] = ugettext("Configure 2FA!")
        return context

    def post(self, request):
        context = self.get_context_data()
        form = TOTPAddDeviceForm(request.POST)
        if form.is_valid():
            # create a new TOTPDevice instance
            token = form.cleaned_data.get("confirmation")
            device = TOTPDevice(
                key=form.cleaned_data.get("key"), name="TOTP", user=request.user
            )
            # validate the confirmation token
            if device.verify_token(token):
                device.save()
                # also add a new StaticDevice with no tokens
                static = StaticDevice(name="Backup token", user=request.user)
                static.save()
                # log the user in so they don't immediately get prompted for
                # another OTP token
                django_otp_login(request, device)
                messages.success(request, ugettext("2FA device successfully added!"))
                return HttpResponseRedirect("/")
            else:
                return self.form_invalid(form)
        else:
            context["form"] = form
            return self.form_invalid(form)


class TOTPRemoveDeviceFormView(GenericFormView):
    """
    Deletes the TOTPDevice, StaticDevice, and all StaticTokens from the user's
    account
    """

    template_name = "deploy/totp_remove_device.html"
    form_class = TOTPRemoveDeviceForm

    def get_context_data(self, **kwargs):
        context = super(GenericFormView, self).get_context_data(**kwargs)
        context["page_title"] = ugettext("Configure 2FA!")
        return context

    def post(self, request):
        form = TOTPRemoveDeviceForm(request.POST)
        if form.is_valid():
            try:
                # loop through the StaticDevices and delete all associated
                # tokens, then delete the StaticDevice
                static_devices = StaticDevice.objects.filter(user_id=request.user.id)
                for device in static_devices:
                    try:
                        StaticToken.objects.filter(device_id=device.id).delete()
                    except StaticToken.DoesNotExist:
                        pass
                    device.delete()
            except StaticDevice.DoesNotExist:
                pass
            try:
                # delete all TOTPDevices
                TOTPDevice.objects.filter(user_id=request.user.id).delete()
            except TOTPDevice.DoesNotExist:
                pass
            messages.success(request, ugettext("Your 2FA devices have been removed!"))
            return HttpResponseRedirect("/totp/add/")


class UpdateFormView(GenericFormView):
    form_class = UpdateForm
    template_name = "deploy/updates.html"

    def get_context_data(self, **kwargs):
        context = super(GenericFormView, self).get_context_data(**kwargs)
        context["page_title"] = ugettext("Updates")
        return context

    def get_initial(self):
        """
        load the initial form value from the db
        """
        data = {}
        daygate = get_config("auto_update_daygate")
        if daygate is not None:
            data["auto_update_daygate"] = literal_eval(daygate)
        pancrypticon = get_config("auto_update_pancrypticon")
        if pancrypticon is not None:
            data["auto_update_pancrypticon"] = literal_eval(pancrypticon)
        return data

    def post(self, request, **kwargs):
        form = UpdateForm(request.POST)
        if form.is_valid():
            auto_update_daygate_value = form.cleaned_data.get("auto_update_daygate")
            auto_update_pancrypticon_value = form.cleaned_data.get(
                "auto_update_pancrypticon"
            )
            set_config("auto_update_daygate", auto_update_daygate_value)
            set_config("auto_update_pancrypticon", auto_update_pancrypticon_value)
            messages.success(
                request, ugettext("Your settings were saved successfully!")
            )
        return self.check_totp(**kwargs)


class VPNClientView(TemplateView):
    template_name = "deploy/vpn_client.html"

    def get_context_data(self, **kwargs):
        context = super(VPNClientView, self).get_context_data(**kwargs)
        if os.path.isfile("/srv/pancrypticon/openvpn/vpnclient.zip"):
            context["file_exists"] = True
        else:
            context["file_exists"] = False
        context["page_title"] = ugettext("VPN Client")
        return context


class PowerControlFormView(GenericFormView):
    form_class = PowerControlForm
    template_name = "deploy/power_control.html"

    def get_context_data(self, **kwargs):
        context = super(GenericFormView, self).get_context_data(**kwargs)
        context["page_title"] = ugettext("Reboot")
        return context

    def post(self, request, **kwargs):
        form = PowerControlForm(request.POST)
        if form.is_valid():
            reboot_value = form.cleaned_data.get("reboot")
            set_config("power_control_reboot", reboot_value)
            messages.success(
                request,
                ugettext("Command sent!")
                + " "
                + ugettext("Please wait a few minutes for the reboot to complete."),
            )
        return self.check_totp(**kwargs)


class NuclearOptionFormView(GenericFormView):
    form_class = NuclearOptionForm
    template_name = "deploy/nuke.html"

    def get_context_data(self, **kwargs):
        context = super(GenericFormView, self).get_context_data(**kwargs)
        context["page_title"] = ugettext("Self-destruct")
        return context

    def get_initial(self):
        """
        set the hidden form field's value
        """
        dw = Diceware()
        return {"hidden_confirmation_code": dw.get_passphrase(length=3)}

    def post(self, request, **kwargs):
        form = NuclearOptionForm(request.POST)
        if form.is_valid():
            set_config("self_destruct", True)
            set_config("self_destruct_confirm", True)
            messages.success(
                request,
                ugettext("Command sent!")
                + " "
                + ugettext("Your server will begin self-destructing momentarily."),
            )
        return self.check_totp(**kwargs)


class StatusView(GenericTemplateView):
    template_name = "deploy/status.html"

    def get_context_data(self, **kwargs):
        tz = pytz.timezone(get_config("timezone") or "UTC")
        dt = datetime.fromtimestamp(float(get_config("status_updated") or 0.0))
        context = super(TemplateView, self).get_context_data(**kwargs)
        context["deployed"] = literal_eval("{}".format(get_config("deployed")))
        context["is_locked"] = literal_eval("{}".format(get_config("status_locked")))
        context["uptime"] = get_config("status_uptime")
        context["docker_status"] = parse_docker_status(get_config("status_docker"))
        context["filesystems"] = parse_filesystem_status(
            get_config("status_filesystems")
        )
        context["daygate_commit"] = get_config("status_daygate_commit")
        context["pancrypticon_commit"] = get_config("status_pancrypticon_commit")
        context["last_updated"] = tz.localize(dt)
        context["page_title"] = ugettext("Server Status")
        return context
