#!/usr/bin/python3
"""
Compares the enabled keys in the database to the authorized_keys file and
replaces the file with the new keys if they have diverged
"""

from hashlib import sha1
from os import chmod, path
import re, sqlite3, stat

authorized_keys_file = "/root/.ssh/authorized_keys"
enabled_keys = ""
authorized_keys = ""
enabled_keys_hash = sha1()
authorized_keys_hash = sha1()

# connect to the database
conn = sqlite3.connect("/home/daygate/daygate/db.sqlite3")
c = conn.cursor()

# query a list of all of the enabled keys
for row in c.execute(
    "SELECT pubkey FROM deploy_sshkey WHERE enabled=1 ORDER BY pubkey"
):
    enabled_keys = enabled_keys + "%s\n" % row
conn.close()
# calculate the sha1 digest of the list
enabled_keys_hash.update(enabled_keys.rstrip().encode("utf-8"))

if path.isfile(authorized_keys_file):
    # slurp in the authorized_keys file
    with open(authorized_keys_file, "r") as f:
        for line in f.readlines():
            m = re.match("^ssh-", line)
            if m:
                authorized_keys = "%s" % line

# calculate the sha1 digest of the list
authorized_keys_hash.update(authorized_keys.rstrip().encode("utf-8"))

# if the digests do not match, overwrite the file and set the mode to 0600
if enabled_keys_hash.hexdigest() != authorized_keys_hash.hexdigest():
    with open(authorized_keys_file, "w") as f:
        f.write(enabled_keys)
    chmod(authorized_keys_file, stat.S_IREAD | stat.S_IWRITE)
