#!/bin/bash

# run PGP signature check before executing the command
cd ~daygate/daygate
/usr/local/bin/daygate-verify-checksums.py && exec "$@"
