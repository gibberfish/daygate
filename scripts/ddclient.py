#!/usr/bin/python3
"""
Update dynamic DNS records
"""

import hashlib, sqlite3, subprocess
from ast import literal_eval
from os import path


def checksum(string):
    hash = hashlib.sha1()
    hash.update(string.encode("utf-8"))
    return hash.hexdigest()


def is_active():
    """
    return True if ddclient is active, else False
    """
    return (
        True
        if subprocess.call(["/bin/systemctl", "is-active", "--quiet", "ddclient"]) == 0
        else False
    )


conf_file = "/etc/ddclient.conf"
# connect to the database
conn = sqlite3.connect("/home/daygate/daygate/db.sqlite3")
c = conn.cursor()
# create a dictionary of nsupdate settings
query = "SELECT key,value FROM deploy_config WHERE key LIKE 'nsupdate_%'"
settings = {}
for (key, value) in c.execute(query):
    settings[key] = value
ddclient_active = is_active()

if literal_eval(settings.get("nsupdate_enabled", "False")):
    conf = (
        "protocol=%s\n" % settings["nsupdate_protocol"]
        + "use=web, web=https://%s/myip\n" % settings["nsupdate_server"]
        + "ssl=yes\n"
        + "server=%s\n" % settings["nsupdate_server"]
        + "login=%s\n" % settings["nsupdate_domain_name"]
        + "password=%s\n" % settings["nsupdate_password"]
        + "%s\n" % settings["nsupdate_domain_name"]
    )
    config_update = False
    stored_checksum = settings.get("nsupdate_conf_hash", None)
    conf_checksum = checksum(conf)
    # compare the checksums to see if our settings have changed
    if (
        stored_checksum is None
        or conf_checksum is None
        or conf_checksum != stored_checksum
    ):
        config_update = True
    # regenerate the config file if it's missing
    elif not path.isfile(conf_file):
        config_update = True

    if config_update:
        # re-write config file
        with open(conf_file, "w") as conf_file:
            conf_file.write(conf)
        # upsert the new file hash
        if stored_checksum is None:
            query = (
                "INSERT INTO deploy_config (key, value) VALUES('nsupdate_conf_hash', ?)"
            )
        else:
            query = (
                "UPDATE deploy_config SET value = ? WHERE key = 'nsupdate_conf_hash'"
            )
        c.execute(query, (conf_checksum,))
        conn.commit()
        # restart ddclient
        subprocess.call(["/bin/systemctl", "restart", "ddclient"])

    if not ddclient_active:
        subprocess.call(["/bin/systemctl", "start", "ddclient"])
elif ddclient_active:
    # turn off ddclient if nsupdate_enabled == False
    subprocess.call(["/bin/systemctl", "stop", "ddclient"])
conn.close()
