#!/usr/bin/python3

import argparse, json, logging, os, random, re, subprocess, sys, socket, sqlite3, time, _thread, queue, hashlib
from websocket import (
    create_connection,
    WebSocketBadStatusException,
    WebSocketConnectionClosedException,
)
from colors import strip_color


class WebSocketClient(object):
    def __init__(self, in_q, out_q, url, msg_timeout=3600):
        self.in_q = in_q
        self.out_q = out_q
        self.url = url
        self.msg_timeout = msg_timeout
        self.connecting = False
        self.connected = False
        self.connection = None
        self.listening = False
        self.sending = False

    def _gen_token(self, length=128):
        """
        generate a hex encoded token string
        """
        random_bytes = os.urandom(length)
        h = hashlib.new("sha256")
        h.update(random_bytes)
        return h.hexdigest()

    def _set_token(self):
        """
        upsert a token string to the database
        """
        logger = logging.getLogger(__name__)
        new_token = self._gen_token()
        logger.debug("Generated new token: %s" % new_token)
        try:
            conn = sqlite3.connect("/home/daygate/daygate/db.sqlite3")
            c = conn.cursor()
            query = "SELECT value FROM deploy_config WHERE key = 'deploy_token'"
            c.execute(query)
            # if it doesn't already exist, do an INSERT...
            if c.fetchone() is None:
                logger.debug("Deploy token does not exist. Creating one.")
                query = "INSERT INTO deploy_config(key,value) VALUES('deploy_token',?)"
            # else do an UPDATE
            else:
                logger.debug("Deploy token exists. Updating it.")
                query = "UPDATE deploy_config SET value=? WHERE key='deploy_token'"
            c.execute(query, (new_token,))
            conn.commit()
            conn.close()
            return new_token
        except sqlite3.Error as e:
            logger.error("An error occurred:", e.args[0])

    def _listen(self):
        logger = logging.getLogger(__name__)
        if not self.connected and not self.connecting:
            self.connect()
        while self.listening:
            try:
                msg = self.connection.recv()
                data = json.loads(msg)
                self.in_q.put(data)
            except (
                socket.error,
                WebSocketBadStatusException,
                WebSocketConnectionClosedException,
            ) as e:
                logger.error("%s. Closing connection" % e)
                time.sleep(3)
                self.connection.close()
                self.connected = False
                self.connect()

    def _send(self):
        logger = logging.getLogger(__name__)
        if not self.connected and not self.connecting:
            self.connect()
        while self.sending:
            try:
                data = self.out_q.get_nowait()
                sys.stdout.write(".")
                self.connection.send(json.dumps(data))
            except socket.error as e:
                logger.error("%s. Could not send data" % e)
            except queue.Empty:
                time.sleep(0.01)
            except (WebSocketBadStatusException, WebSocketConnectionClosedException):
                time.sleep(3)
                self.connection.close()
                self.connected = False
                self.connect()

    def connect(self):
        logger = logging.getLogger(__name__)
        # connect to the websocket
        logger.debug("Establishing websocket connection to %s ..." % self.url)
        self.connecting = True
        while not self.connected:
            # reset the token on every (re-)connection
            token = self._set_token()
            try:
                ws = create_connection("%s?token=%s" % (self.url, token))
                self.connected = True
                self.connection = ws
                logger.debug("Connection established")
            except (
                socket.error,
                WebSocketBadStatusException,
                WebSocketConnectionClosedException,
            ) as e:
                logger.error("Connection failed. Retrying in 5 seconds")
                logger.error("Failure message: %s" % str(e))
                time.sleep(5)
        self.connecting = False

    def start_listening(self):
        self.listening = True
        _thread.start_new_thread(self._listen, ())

    def stop_listening(self):
        self.listening = False

    def start_sending(self):
        self.sending = True
        _thread.start_new_thread(self._send, ())

    def stop_sending(self):
        self.sending = False

    def start(self):
        self.start_listening()
        time.sleep(0.5)
        self.start_sending()

    def stop(self):
        self.stop_listening()
        self.stop_sending()


class DeployOMatic(object):
    def __init__(self, in_q, out_q, msg_timeout=3600):
        self.in_q = in_q
        self.out_q = out_q
        self.msg_timeout = msg_timeout
        self.process_queue = False
        self.run_lock = False

    def run_script(self, command, env_vars):
        logger = logging.getLogger(__name__)
        try:
            if self.run_lock:
                """
                if the script is already running, backfill the current log
                contents to the queue
                """
                self.out_q.put("Another script is already running")
                try:
                    with open("/home/daygate/deploy-o-matic.log", "r") as log:
                        for line in log:
                            m = re.match(
                                "[0-9-]+\s[0-9:,]+\s[A-Z]+\s(.*)$", line.rstrip()
                            )
                            if m:
                                self.out_q.put(
                                    {"action": "print", "message": m.group(1)}
                                )
                except Exception as e:
                    logger.debug("Can't open log: %s" % str(e))
                    pass
            else:
                self.run_lock = True
                _thread.start_new_thread(self.show_progress, (), {"seconds": 1800})
                p = subprocess.Popen(
                    command,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                    env=env_vars,
                )
                for line in iter(p.stdout.readline, ""):
                    if not len(line):
                        break
                    elif type(line) is not str:
                        line = line.decode("utf-8")
                    output = {"action": "print", "message": strip_color(line.rstrip())}
                    self.out_q.put(output)
                    logger.info(output["message"])
                self.run_lock = False
        except RuntimeError:
            self.run_lock = False
            raise

    def show_progress(self, seconds=3600, fudge=True):
        interval = seconds / 1000
        self.out_q.put({"action": "start_progress"})
        while self.run_lock:
            if fudge:
                fudge_factor = random.randint(0, int(interval * 0.5)) * (
                    1 * random.randint(-1, 1)
                )
            self.out_q.put({"action": "increment_progress"})
            time.sleep(interval + fudge_factor)
        self.out_q.put({"action": "stop_progress"})

    def _process_in_q(self):
        """
        pop messages off the queue and do something with them
        """
        while self.process_queue:
            try:
                data = self.in_q.get_nowait()
                if data["action"] == "launch":
                    _thread.start_new_thread(
                        self.run_script,
                        (
                            ["/usr/bin/sudo", "--preserve-env", "scripts/launcher.sh"],
                            {
                                "ADMIN_EMAIL": data["admin_email"],
                                "ADMIN_PASSWORD": data["admin_passphrase"],
                                "ENCRYPTION_PASSPHRASE": data["passphrase"],
                                "HIDDEN_DOMAIN_NAME": data["hidden_domain_name"],
                                "NEXTCLOUD_DOMAIN_NAME": data["domain_name"],
                                "OFFICE_DOMAIN_NAME": data["office_domain_name"],
                                "CONFERENCE_DOMAIN_NAME": data[
                                    "conference_domain_name"
                                ],
                                "NEXTCLOUD_LANGUAGE": data["language"],
                                "TESTING": data["testing"],
                                "GIT_BRANCH": data["git_branch"],
                                "FORCE_REBUILD": data["force_rebuild"],
                            },
                        ),
                    )
                elif data["action"] == "print" or re.match(
                    "^(stop|start|increment)_progress", data["action"]
                ):
                    pass
                else:
                    logger.error(
                        "The server wants me to '%s'. \
                        I don't know how to do that!"
                        % data["action"]
                    )
            except ValueError as e:
                if str(e) == "No JSON object could be decoded":
                    pass
                else:
                    raise
            except queue.Empty:
                time.sleep(0.01)

    def start(self):
        self.process_queue = True
        _thread.start_new_thread(self._process_in_q, ())

    def stop(self):
        self.process_queue = False


def now(offset=0):
    return int(time.time()) + offset


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", help="Websocket host address")
    parser.add_argument(
        "--no_ssl",
        help="Disable secure connections",
        action="store_true",
        default=False,
    )
    args = parser.parse_args()
    in_q = queue.Queue()
    out_q = queue.Queue()
    logger = logging.getLogger(__name__)
    logging.basicConfig(
        level="DEBUG",
        filemode="w",
        filename="/home/daygate/deploy-o-matic.log",
        format="%(asctime)s %(levelname)s %(message)s",
    )
    logger.info("Starting up...")

    # start the communications handler
    if args.no_ssl:
        proto = "ws"
    else:
        proto = "wss"

    wsclient = WebSocketClient(in_q, out_q, "%s://%s/ws/" % (proto, args.host))
    dom = DeployOMatic(in_q, out_q)
    dom.start()

    restart_timer = 0
    while True:
        sys.stdout.flush()
        # restart wsclient connection after 1 hour if idle
        if now() > restart_timer and dom.run_lock == False:
            logger.debug("Idle timeout reached. Restarting ws client...")
            wsclient.stop()
            wsclient.connect()
            wsclient.start()
            restart_timer = now(offset=3600)
        else:
            time.sleep(1)


if __name__ == "__main__":
    main()
