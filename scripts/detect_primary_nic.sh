#!/usr/bin/env bash

default_gateways=`/sbin/ip route | /bin/grep "default via" | \
    /usr/bin/awk '{print $5}'`
metric=9999
primary=''

for d in $default_gateways; do
  m=`/sbin/ip route | /bin/grep "default via" | /bin/grep "dev ${d}" | \
      /usr/bin/awk '{print $9}'`
  if [[ m -lt metric ]]; then
    metric=$m
    primary=$d
  fi
done

/bin/echo $primary
