#!/usr/bin/env bash

devices=`/bin/ls -l /sys/class/net/ | /bin/grep "devices/pci" | \
    /usr/bin/awk '{print $9}'`

for d in $devices; do
  if `/sbin/ip link show $d | /bin/grep "state DOWN" | \
      /bin/grep -vq "SLAVE"`; then
    /bin/echo $d
    exit 0
  fi
done
