#!/bin/bash

/bin/echo "Filling $u with encrypted noise (this may take a while)..."

/usr/bin/openssl enc -aes-256-ctr -pass \
    pass:"$(/bin/dd if=/dev/urandom bs=128 count=1 2> /dev/null | \
    /usr/bin/base64)" -nosalt < /dev/zero | \
    /bin/dd bs=64K of=$1
