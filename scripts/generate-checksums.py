#!/usr/bin/python3

import json, os, re
from hashlib import sha256


def checksum(filename):
    """
    returns a sha256 hash for a given file
    """
    hash_sha256 = sha256()
    with open(filename, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_sha256.update(chunk)
    return hash_sha256.hexdigest()


checksums = {}
# get a list of all the files tracked by git
with os.popen("/usr/bin/git ls-files") as f:
    for line in f.readlines():
        filename = line.rstrip()
        # calculate checksums for all files except the checksum files
        if not re.match("checksums\.json(\.asc)?", filename):
            checksums[filename] = checksum(filename)

# dump to a file in JSON format
with open("checksums.json", "w") as outfile:
    json.dump(checksums, outfile, sort_keys=True, indent=4)
