#!/bin/bash

# detect system architecture
ARCH=`/usr/bin/dpkg --print-architecture`

export DEBIAN_FRONTEND=noninteractive

if [ -z $GIT_BRANCH ]; then
    GIT_BRANCH="master"
fi

primary_nic=$1
secondary_nic=$2

# set primary (external) NIC devname
if [ -z "$primary_nic" ]; then
    primary_nic=`scripts/detect_primary_nic.sh`
fi

if [ -z "$secondary_nic" ]; then
    detected_secondary_nic=`scripts/detect_secondary_nic.sh`
    if [ -n "$detected_secondary_nic" ]; then
        if [[ "$detected_secondary_nic" != "$primary_nic" ]]; then
          secondary_nic=$detected_secondary_nic
        fi
    fi
fi

# make sure swap is disabled
/sbin/swapoff -a

/bin/echo "- Installing packages..."
/usr/bin/apt-get update && \
/usr/bin/apt-get install -y tor
/bin/systemctl enable tor
/bin/systemctl start tor
/usr/bin/torsocks /usr/bin/apt-get -y upgrade && \
/usr/bin/torsocks /usr/bin/apt-get install -y \
    apt-transport-https \
    bind9utils \
    cifs-utils \
    ddclient \
    dnsmasq \
    ca-certificates \
    gettext \
    git \
    gnupg2 \
    libffi-dev \
    libjpeg-dev \
    libssl-dev \
    nginx \
    ntp \
    pwgen \
    python3-dev \
    python3-pip \
    rng-tools \
    rsync \
    shorewall \
    shorewall6 \
    software-properties-common \
    sqlite3 \
    tor \
    zlib1g-dev

/bin/systemctl stop ddclient nginx
/bin/systemctl enable nginx shorewall shorewall6

# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=808296#25
if [ ! -d /etc/systemd/system/tor@default.service.d/ ]; then
    /bin/mkdir /etc/systemd/system/tor@default.service.d/
    (/bin/echo "[Service]"; /bin/echo "AppArmorProfile=") > \
        /etc/systemd/system/tor@default.service.d/override.conf
    /bin/systemctl daemon-reload
    /bin/systemctl start tor
    # give tor time to bootstrap
    /bin/sleep 15
fi

if [[ `/bin/egrep -c '^daygate:x:' /etc/passwd` -eq '0' ]]; then
    /bin/echo "- Creating 'daygate' user..."
    /usr/sbin/useradd -m daygate -s /sbin/nologin
fi

/bin/chown daygate /etc/ddclient.conf

cd ~daygate

if [ ! -d /home/daygate/daygate ]; then
    /bin/echo "- Cloning git repo..."
    /usr/bin/torsocks /usr/bin/git clone -b $GIT_BRANCH https://gitlab.com/gibberfish/daygate.git && \
    cd daygate && \
    scripts/install.sh $@
    exit
fi

/bin/echo "- Installing dependencies..."
cd ~daygate/daygate && \
/bin/cp misc/resolv.conf /etc/resolv.conf && \
/usr/bin/torsocks /usr/bin/git pull && \
/usr/bin/torsocks /usr/bin/pip3 install -U setuptools
/usr/bin/torsocks /usr/bin/pip3 install -U docker-compose
/usr/bin/torsocks /usr/bin/pip3 install -r requirements.txt

/bin/echo "- Installing Docker..."
  /usr/bin/apt-key add misc/docker-debian.gpg
  if [[ ! -f /etc/apt/sources.list.d/docker.list ]]; then
      if [[ "$ARCH" == 'armhf' ]]; then
          # apt-add-repository on raspbian chokes on this, but adding the repo
          # manually works fine... 🙄
          /bin/echo "deb [arch=armhf] \
              https://download.docker.com/linux/$(. /etc/os-release; /bin/echo "$ID") \
              $(lsb_release -cs) stable" | \
              /usr/bin/tee /etc/apt/sources.list.d/docker.list
      else
          # the proper method:
          /usr/bin/add-apt-repository \
              "deb [arch=${ARCH}] https://download.docker.com/linux/debian \
              $(lsb_release -cs) stable"
      fi
  fi
  /usr/bin/torsocks /usr/bin/apt-get update && \
  /usr/bin/torsocks /usr/bin/apt-get install -y docker-ce

/bin/echo "Copying security scripts..."
# move these scripts out of the repo to prevent them from being overwritten
# by a pull
/bin/cp scripts/daygate-verify-checksums.py /usr/local/bin/
/bin/cp scripts/daygate_exec.sh /usr/local/bin/
/bin/chown -R daygate.daygate /dev/shm

if [ ! -f daygate/local_settings.py ]; then
    /bin/cp daygate/local_settings.py.example daygate/local_settings.py
fi

/bin/echo "- Configuring firewall..."
/bin/cp misc/shorewall/* /etc/shorewall/
/bin/cp misc/shorewall/* /etc/shorewall6/
/bin/sed -i 's|ipv4|ipv6|g' /etc/shorewall6/zones
/bin/sed -i "s|PRIMARY_NIC|${primary_nic}|g" /etc/shorewall/interfaces
/bin/sed -i "s|PRIMARY_NIC|${primary_nic}|g" /etc/shorewall6/interfaces
/bin/sed -i 's|DOCKER=No|DOCKER=Yes|g' /etc/shorewall/shorewall.conf
/bin/sed -i 's|DOCKER=No|DOCKER=Yes|g' /etc/shorewall6/shorewall6.conf
# set up NAT and DHCP if two nics are defined
if [ -z $secondary_nic ]; then
    /bin/rm /etc/shorewall/masq
else
    # configure LAN interface
    /bin/cp misc/interfaces.d/lan /etc/network/interfaces.d/
    /bin/sed -i "s|SECONDARY_NIC|${secondary_nic}|g" /etc/network/interfaces.d/lan
    /sbin/ifup $secondary_nic
    # configure DHCP
    /bin/cp misc/dnsmasq.conf /etc/
    /bin/sed -i "s|SECONDARY_NIC|${secondary_nic}|g" /etc/dnsmasq.conf
    /bin/systemctl enable dnsmasq
    /bin/systemctl restart dnsmasq
    # configure NAT
    /bin/echo "${primary_nic}   ${secondary_nic}" > /etc/shorewall/masq
    /bin/sed -i "s|^#loc|loc|g" /etc/shorewall/zones
    /bin/sed -i "s|^#loc|loc|g" /etc/shorewall6/zones
    /bin/sed -i "s|^#loc|loc|g" /etc/shorewall/interfaces
    /bin/sed -i "s|^#loc|loc|g" /etc/shorewall6/interfaces
    /bin/sed -i "s|^#loc|loc|g" /etc/shorewall/policy
    /bin/sed -i "s|^#loc|loc|g" /etc/shorewall6/policy
    /bin/sed -i "s|^#ACCEPT          loc|ACCEPT          loc|g" /etc/shorewall/rules
    /bin/sed -i "s|^#ACCEPT          loc|ACCEPT          loc|g" /etc/shorewall6/rules
    /bin/sed -i "s|SECONDARY_NIC|${secondary_nic}|g" /etc/shorewall/interfaces
    /bin/sed -i "s|SECONDARY_NIC|${secondary_nic}|g" /etc/shorewall6/interfaces
fi
/sbin/shorewall check && /sbin/shorewall restart
/sbin/shorewall6 check && /sbin/shorewall6 restart

/bin/echo "Importing Gibberfish Security PGP key..."
/usr/bin/gpg --import -v misc/security-pubkey.asc
/bin/su -s /bin/bash -c "/usr/bin/gpg --import -v misc/security-pubkey.asc" \
    daygate

/bin/cp /home/daygate/daygate/misc/snapshot_backup.cronjob /etc/cron.d/snapshot_backup
/bin/cp misc/daygate.cronjob /etc/cron.d/daygate
/bin/sed -i "s|PRIMARY_NIC|$primary_nic|g" /etc/cron.d/daygate
/bin/sed -i "s|SECONDARY_NIC|$secondary_nic|g" /etc/cron.d/daygate

/bin/cp misc/pancrypticon.cronjob /etc/cron.d/pancrypticon
# remove .cronjob files from previous iterations
/bin/rm /etc/cron.d/*.cronjob /etc/cron.d/ddclient

if [[ `/bin/egrep -c '^PANCRYPTICON_GIT_BRANCH' daygate/local_settings.py` -eq '0' ]]; then
    /bin/echo "- Setting pancrypticon git branch to ${GIT_BRANCH}..."
    cd ~daygate/daygate && \
    /bin/echo "PANCRYPTICON_GIT_BRANCH='${GIT_BRANCH}'" >> daygate/local_settings.py
fi

if [[ `/bin/egrep -c '^SECRET_KEY' daygate/local_settings.py` -eq '0' ]]; then
    /bin/echo "- Generating secret key..."
    cd ~daygate/daygate && \
    secret_key=`/usr/bin/pwgen -s 64 -n 1`
    /bin/echo "SECRET_KEY='${secret_key}'" >> daygate/local_settings.py
fi

if [[ ! -z $SELF_HOSTED && \
    `/bin/egrep -c '^SELF_HOSTED' daygate/local_settings.py` -eq '0' ]]; then
    /bin/echo "SELF_HOSTED='${SELF_HOSTED}'" >> daygate/local_settings.py
fi

if [[ `/bin/egrep -c '^HiddenService' /etc/tor/torrc` -eq '0' ]]; then
    /bin/echo "- Configuring Tor..."
    /bin/mkdir -p /var/lib/tor/hidden_service/daygate
    /bin/mkdir -p /var/lib/tor/hidden_service/pancrypticon
    /bin/chown -R debian-tor.debian-tor /var/lib/tor/hidden_service
    /bin/chmod 700 /var/lib/tor/hidden_service/*
    /bin/cat misc/tor-hidden-service.conf >> /etc/tor/torrc && \
    /bin/systemctl restart tor
fi

/bin/echo "- Configuring sudoers..."
/bin/cp misc/daygate.sudoers.cfg /etc/sudoers.d/daygate

/bin/echo "- Configuring nginx..."
/bin/cp misc/nginx-default.conf /etc/nginx/sites-enabled/default && \
/bin/systemctl restart nginx

if [ ! -f /root/.ssh/backups ]; then
    /bin/mkdir -m 0700 -p /root/.ssh
    /bin/echo "- Generating backup keypair..."
    /usr/bin/ssh-keygen -N '' -f /root/.ssh/backups && \
    /bin/cp /root/.ssh/backups.pub /home/daygate
fi

/bin/echo "- Collecting static files..."
/bin/echo "yes" | ./manage.py collectstatic

/bin/echo "- Initializing database..."
if [ ! -f db.sqlite3 ]; then
    ./manage.py migrate
    ./manage.py loaddata config portal_admin
fi
/bin/chmod 0600 db.sqlite3
./manage.py migrate
`/usr/bin/which django-admin` compilemessages

/bin/cp misc/logrotate.conf /etc/
/bin/cp misc/logrotate.d/* /etc/logrotate.d/

/bin/echo "- Starting services..."
/bin/cp misc/systemd/* /etc/systemd/system && \
/bin/systemctl daemon-reload && \
/bin/systemctl enable daygate-daphne daygate-worker daygate-deploy && \
/bin/systemctl stop daygate-daphne daygate-worker daygate-deploy
/bin/systemctl start daygate-daphne daygate-worker daygate-deploy

if [ -f db.sqlite3 ]; then
    domain_name=`/bin/echo "SELECT value FROM deploy_config WHERE key = 'base_domain';" | \
        /usr/bin/sqlite3 db.sqlite3`
else
    domain_name=''
fi
hidden_service=`/bin/cat /var/lib/tor/hidden_service/daygate/hostname`
nextcloud_tor=`/bin/cat /var/lib/tor/hidden_service/pancrypticon/hostname`
if [[ `/bin/egrep -c '^HIDDEN_DOMAIN_NAME' daygate/local_settings.py` -eq '0' ]]; then
    /bin/echo "HIDDEN_DOMAIN_NAME='${hidden_service}'" >> daygate/local_settings.py
fi
if [[ `/bin/egrep -c '^NEXTCLOUD_HIDDEN_DOMAIN_NAME' daygate/local_settings.py` -eq '0' ]]; then
    /bin/echo "NEXTCLOUD_HIDDEN_DOMAIN_NAME='${nextcloud_tor}'" >> daygate/local_settings.py
fi
if [[ `/bin/egrep -c '^ALLOWED_HOSTS' daygate/local_settings.py` -eq '0' ]]; then
    /bin/echo "ALLOWED_HOSTS = [HIDDEN_DOMAIN_NAME, 'localhost']" >> daygate/local_settings.py
fi

/bin/echo $hidden_service

/bin/chown -R daygate.daygate ~daygate/daygate
/bin/cp misc/pancrypticon.cronjob /etc/cron.d/pancrypticon

# set up login banner to display URLs
/bin/cp misc/issue /etc/
/bin/sed -i "s|NEXTCLOUD_DOMAIN_NAME|$domain_name|" /etc/issue
/bin/sed -i "s|NEXTCLOUD_TOR|$nextcloud_tor|" /etc/issue
/bin/sed -i "s|MANAGEMENT_PORTAL|$hidden_service|" /etc/issue
if [[ `/bin/egrep -c '^Banner' /etc/ssh/sshd_config` -eq '0' ]]; then
    /bin/echo "" >> /etc/ssh/sshd_config
    /bin/echo "Banner /etc/issue" >> /etc/ssh/sshd_config
    /bin/systemctl restart sshd
fi
