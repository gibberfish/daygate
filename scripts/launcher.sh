#!/bin/bash

## automatically encrypts unused block devices and adds them to an LVM volgroup
## expands filesystems as necessary, then deploys and configures Pancrypticon

OPT_SIZE='20G'

# set swap size to 25% of RAM, or 4GB, whichever is less, minimum 1GB
MEM_TOTAL=`/usr/bin/free --giga | /bin/egrep '^Mem:'| /usr/bin/awk '{print $2}'`
SWAP_CUR=`/usr/bin/free --giga | /bin/egrep '^Swap:'| /usr/bin/awk '{print $2}'`
if [[ $"SWAP_CUR" -eq 0 ]]; then
    SWAP_G=$(("$MEM_TOTAL"/4))
    if [[ "$SWAP_G" -gt 4 ]]; then
        SWAP_SIZE="4G"
    elif [[ "$SWAP_G" -lt 1 ]]; then
        SWAP_SIZE="1G"
    else
        SWAP_SIZE="${SWAP_G}G"
    fi
fi

# detect system architecture
ARCH=`/usr/bin/dpkg --print-architecture`

export DEBIAN_FRONTEND='noninteractive'

function install_deps() {
    /usr/bin/apt-get update -q
    /usr/bin/apt-get install -qy tor
    /sbin/systemctl enable tor
    /sbin/systemctl start tor
    /bin/sleep 10
    /usr/bin/torsocks /usr/bin/apt-get install -qy \
        cryptsetup \
        ecryptfs-utils \
        lvm2 \
        pwgen \
        python-pip \
        sqlite3 \
        virtualenv \
        xfsprogs
}

function create_keyfile() {
    local passphrase=$1
    local keyfile=$2
    /bin/echo -n "passphrase_passwd=${passphrase}" > $keyfile
}

function sanitize_passphrase() {
    # strip leading/trailing spaces, replace control characters with spaces,
    # then truncate to 64 chars due to a length limitation imposed by eCryptfs
    /bin/echo -n "$1" | \
        /bin/sed 's|^[[:space:]]\+||' | \
        /bin/sed 's|[[:space:]]\+$||' | \
        /bin/sed 's|[[:cntrl:]]| |g' | \
        /usr/bin/cut -c -64
}

function luks_open() {
    local luks_device=$1
    local volname=$2
    # unlock the encrypted volume
    /sbin/cryptsetup open --type luks --keyfile-offset 18 --key-file \
        /mnt/ramdisk/keyfile $luks_device $volname && \
    # in case the block device's size has grown, try to expand it
    /sbin/cryptsetup resize $volname
}

function mount_ramdisk() {
    # generate a one-off encryption passphrase
    tmp_pass=`/usr/bin/pwgen -s 32 -n 1`
    # create and encrypt a 100MB ramdisk
    /sbin/rmmod -f brd && /bin/rm /dev/ram0
    /sbin/modprobe brd rd_size=102400 && \
    /sbin/blockdev --flushbufs /dev/ram0
    /bin/echo -n $tmp_pass | /sbin/cryptsetup -s 512 luksFormat /dev/ram0 - && \
    /bin/echo -n $tmp_pass | /sbin/cryptsetup open --type luks --key-file - \
            /dev/ram0 ram0 && \
    # initialize the disk with random noise
    /bin/dd if=/dev/urandom of=/dev/ram0 bs=4096 count=512 && \
    # create a filesystem and mount it
    /sbin/mkfs.ext2 /dev/mapper/ram0 > /dev/null
    /bin/mkdir -p /mnt/ramdisk && \
    /bin/mount /dev/mapper/ram0 /mnt/ramdisk && \
    /bin/chmod 750 /mnt/ramdisk
}

function umount_ramdisk() {
    # unmount, close, and wipe the ramdisk
    /bin/umount /mnt/ramdisk
    /sbin/cryptsetup luksClose ram0
    /bin/dd if=/dev/urandom of=/dev/ram0 bs=4096 count=512
}

function git_clone() {
    local dest=$1
    cd $dest && \
    if [ ! -d pancrypticon ]; then
        /usr/bin/torsocks /usr/bin/git clone -b $GIT_BRANCH \
            https://gitlab.com/gibberfish/pancrypticon
    fi
    cd pancrypticon && cp build/ejabberd/files/initdb.d/* initdb.d/ && \
    /bin/chown 999 initdb.d
}

function croak(){
    umount_ramdisk
    exit 255
}

function create_env_file() {
    local filename=$1
    local turn_secret=`/usr/bin/pwgen -s 64 -n 1`
    local mysql_password=`/usr/bin/pwgen -s 32 -n 1`
    local mysql_root_password=`/usr/bin/pwgen -s 32 -n 1`
    local ojsxc_token=`/usr/bin/pwgen -s 23 -n 1`

    /bin/echo -n "" > $filename && \
    /bin/echo "NEXTCLOUD_DOMAIN_NAME=$NEXTCLOUD_DOMAIN_NAME" >> $filename
    /bin/echo "OFFICE_DOMAIN_NAME=$OFFICE_DOMAIN_NAME" >> $filename
    /bin/echo "CONFERENCE_DOMAIN_NAME=$CONFERENCE_DOMAIN_NAME" >> $filename
    /bin/echo "HIDDEN_DOMAIN_NAME=$HIDDEN_DOMAIN_NAME" >> $filename
    /bin/echo "ADMIN_EMAIL=$ADMIN_EMAIL" >> $filename
    /bin/echo "ADMIN_PASSWORD=$ADMIN_PASSWORD" >> $filename
    /bin/echo "MYSQL_USER=pancrypticon" >> $filename
    /bin/echo "MYSQL_PASSWORD=$mysql_password" >> $filename
    /bin/echo "MYSQL_ROOT_PASSWORD=$mysql_root_password" >> $filename
    /bin/echo "TURN_SERVER=${NEXTCLOUD_DOMAIN_NAME}:5349" >> $filename
    /bin/echo "TURN_SECRET=$turn_secret" >> $filename
    /bin/echo "OJSXC_TOKEN=$ojsxc_token" >> $filename
    /bin/echo "NEXTCLOUD_THEME=gibberfish" >> $filename
    /bin/echo "LANGUAGE=$NEXTCLOUD_LANGUAGE" >> $filename
    if [ -n "$SELF_HOSTED" ]; then
        /bin/echo "SELF_HOSTED=$SELF_HOSTED" >> $filename
    fi
    if [ -n "$TESTING" ]; then
        /bin/echo "TESTING=$TESTING" >> $filename
    fi
}

# make sure a passphrase was set
if [ -z "$ENCRYPTION_PASSPHRASE" ]; then
    /bin/echo "ENCRYPTION_PASSPHRASE may not be zero length!" && croak
fi

if [ -z "$NEXTCLOUD_DOMAIN_NAME" ]; then
    /bin/echo "NEXTCLOUD_DOMAIN_NAME may not be zero length!" && croak
fi

if [ -z "$OFFICE_DOMAIN_NAME" ]; then
    /bin/echo "OFFICE_DOMAIN_NAME may not be zero length!" && croak
fi

if [ -z "$CONFERENCE_DOMAIN_NAME" ]; then
    /bin/echo "CONFERENCE_DOMAIN_NAME may not be zero length!" && croak
fi

if [ -z "$ADMIN_EMAIL" ]; then
    /bin/echo "ADMIN_EMAIL may not be zero length!" && croak
fi

if [ -z "$ADMIN_PASSWORD" ]; then
    /bin/echo "ADMIN_PASSWORD may not be zero length!" && croak
fi

if [ -z "$NEXTCLOUD_LANGUAGE" ]; then
    NEXTCLOUD_LANGUAGE="en"
fi

# don't spam the console with warnings, etc
#exec 2> storage_setup.err

start_time=`/bin/date`
SECONDS=0

/usr/bin/printf "### Deployment started $start_time ###\n\n"

/bin/systemctl stop docker > /dev/null 2>&1

/bin/echo "- Installing prerequisites..."
install_deps > /dev/null 2>&1

/bin/echo "- Scanning block devices..."

/sbin/blkid -g

passphrase=$(sanitize_passphrase $ENCRYPTION_PASSPHRASE)
# find all existing LUKS volumes
luks_vols=(`/sbin/blkid | /bin/grep 'TYPE="crypto_LUKS"' | \
    /usr/bin/awk -F ':' '{print $1}'`)
# find all existing unused volumes
unused=(`/sbin/blkid | /bin/grep -v 'mmcblk' | /bin/grep -v ' TYPE=' | \
    /bin/grep -v 'PARTLABEL="BIOS"' | /bin/grep -v 'PTTYPE="gpt"' | \
    /usr/bin/awk -F ':' '{print $1}'`)
counter=0

/bin/echo "  * Found LUKS volumes: ${luks_vols[@]}"
/bin/echo "  * Found unused block devices: ${unused[@]}"

if [ ${#unused[@]} -gt 0 ] || [ ${#luks_vols[@]} -gt 0 ]; then
    # luks/ecryptfs needs to read the password from a file, so create an
    # encrypted ramdisk to store it temporarily
    /bin/echo "- Initializing encrypted ramdisk..."
    mount_ramdisk && \
    create_keyfile $passphrase /mnt/ramdisk/keyfile
fi

# loop over the unuzed volumes and luksFormat them
if [ ${#unused[@]} -gt 0 ]; then
    for u in "${unused[@]}"; do
        # double check that this is not already a LUKS volume
        if [ ! `/sbin/cryptsetup isLuks $u` ]; then
            # /bin/echo \
            #     "- Filling $u with encrypted noise (this may take a while)..."
            # # pre-fill the device with AES256 encrypted noise
            # /usr/bin/openssl enc -aes-256-ctr -pass \
            #     pass:"$(/bin/dd if=/dev/urandom bs=128 count=1 2> /dev/null | \
            #     /usr/bin/base64)" -nosalt < /dev/zero | \
            #     /bin/dd bs=64K of=$u && \

            /bin/echo "- Formatting $u as a LUKS volume"
            # set up the unused device as a LUKS volume
            /sbin/cryptsetup -s 512 luksFormat --batch-mode \
                --keyfile-offset 18 $u /mnt/ramdisk/keyfile
        else
            /bin/echo "  * $u is already a LUKS volume -- skipping"
        fi
        # add this to the list of existing LUKS volumes
        luks_vols=(${luks_vols[@]} $u)
    done
fi

# loop over the LUKS volumes and attempt to open them using the passphrase
if [ ${#luks_vols[@]} -gt 0 ]; then
    /bin/echo "- Opening LUKS volumes..."
    for l in ${luks_vols[@]}; do
        volname="luksvol${counter}"
        counter=$[$counter + 1]

        # make sure this volume is inactive before trying to open it
        /sbin/cryptsetup status $volname > /dev/null 2>&1

        if [ $? -gt 0 ]; then
            /bin/echo "  * Opening $l as $volname"
            luks_open $l $volname || \
            /bin/echo "  * Opening $volname failed! Is the passphrase correct?"
        else
            /bin/echo "  * $volname already active -- skipping"
        fi

        # if this volume isn't already an LVM Physical Volume, make it one
        if [ ! -d /dev/mapper/$volname ]; then
            if [ `/sbin/pvs | /bin/grep -c "/dev/mapper/${volname}"` == 0 ];
            then
                /bin/echo "  * Creating new Physical Volume from ${volname}..."
                /sbin/pvcreate /dev/mapper/${volname}
            fi

            # if the 'pancrypticon' Volume Group does not exist,
            # create it with this PV
            if [ `/sbin/vgs | /bin/egrep -c '^\s+pancrypticon\s+'` == 0 ]; then
                /bin/echo "  * Creating new Volume Group 'pancrypticon'..."
                /sbin/vgcreate --yes pancrypticon \
                    /dev/mapper/${volname} > /dev/null 2>&1
            # else extend the existing VG with this PV
            elif [ `/sbin/pvs | /bin/grep -c "${volname} pancrypticon"` == 0 ];
            then
                /bin/echo "  * Adding ${volname} to VG 'pancrypticon'..."
    	        /sbin/vgextend --yes pancrypticon \
                    /dev/mapper/${volname} > /dev/null 2>&1

            fi
        fi
    done

    if [ `/sbin/vgs | /bin/egrep -c '^\s+pancrypticon\s+'` -gt 0 ]; then
        # if there is no 'swap' Logical Volume, create it and mkswap
        if [ `/sbin/lvs | /bin/egrep -c '^\s+swap\s+'` == 0 ]; then
            if [ -n $SWAP_SIZE ]; then
                /bin/echo "- Creating Logical Volume 'swap'..."
                /sbin/lvcreate --yes pancrypticon -n swap -L+"${SWAP_SIZE}" && \
                /usr/bin/yes | /sbin/mkswap /dev/pancrypticon/swap
            fi
        fi

        # if there is no 'opt' Logical Volume, create it and mkfs
        if [ `/sbin/lvs | /bin/egrep -c '^\s+opt\s+'` == 0 ]; then
            /bin/echo "- Creating Logical Volume 'opt'..."
            /sbin/lvcreate --yes pancrypticon -n opt -L+$OPT_SIZE && \
            /usr/bin/yes | /sbin/mkfs.xfs -q /dev/pancrypticon/opt
        fi

        # if there is no 'srv' LV, create it using 95% of the remaining
        # free space and mkfs
        if [ `/sbin/lvs | /bin/egrep -c '^\s+srv\s+'` == 0 ]; then
            /bin/echo "- Creating Logical Volume 'srv'..."
            /sbin/lvcreate --yes pancrypticon -n srv -l+95%FREE && \
            /usr/bin/yes | /sbin/mkfs.xfs -q /dev/pancrypticon/srv
        fi

        # calculate the amount of unallocated space on the Volume Group
        unallocated=`/sbin/vgs pancrypticon --units G --rows | /bin/grep VFree | \
            /usr/bin/awk '{print $2}' | /usr/bin/awk -F '.' '{print $1}'`
        # reserve 5G per PV and subtract this from the unallocated space
        allocate=`/bin/echo "${unallocated} - (${#luks_vols[@]} * 5)" | bc -ql`

        # if the number above is > 0, expand the 'srv' LV and resize the filesystem
        if [ $allocate -gt 0 ]; then
            /bin/echo "Extending Logical Volume 'srv'..."
            /sbin/lvresize --yes --resizefs -L+${allocate}G \
                /dev/pancrypticon/srv
        fi

        /bin/echo "- Activating swap..."
        /sbin/swapon /dev/pancrypticon/swap

        /bin/echo "- Mounting filesystems..."
        if ! /bin/mountpoint -q /opt; then
            /bin/mount /dev/pancrypticon/opt /opt || croak
            /bin/echo "  * /opt mounted"
        else
            /bin/echo "  * /opt already mounted -- skipping"
        fi

        if ! /bin/mountpoint -q /srv; then
            /bin/mount /dev/pancrypticon/srv /srv || croak && \
            /bin/echo "  * /srv mounted"
            /bin/echo "* Overlaying /srv with eCryptfs..."

            # mount the ecryptfs overlay on top of /srv
            /usr/bin/printf "%s" $passphrase | \
                /usr/bin/ecryptfs-add-passphrase --fnek - && \
            /bin/echo '' | /bin/mount -t ecryptfs -o key=passphrase:passphrase_passwd_file=/mnt/ramdisk/keyfile,ecryptfs_cipher=aes,ecryptfs_key_bytes=32,ecryptfs_passthrough=n,ecryptfs_enable_filename_crypto=y,no_sig_cache=y /srv /srv || croak
        else
            /bin/echo "  * /srv already mounted -- skipping"
        fi

        # unmount and wipe ramdisk
        umount_ramdisk

        mkdir -p /opt/pancrypticon/docker /srv/pancrypticon
        /bin/echo \
            '{"graph":"/opt/pancrypticon/docker","storage-driver":"overlay2"}' \
            > /etc/docker/daemon.json

        # clone pancrypticon repo to /opt/pancrypticon
        /bin/echo "- Cloning pancrypticon to /opt..."
        if [ ! -d /opt/pancrypticon/pancrypticon ]; then
            git_clone /opt/pancrypticon && \
            /bin/echo "  * Complete"
        # or pull fresh code
        else
            CONFIG_DB="/home/daygate/daygate/db.sqlite3"
            UPDATES_ENABLED=`/bin/echo "SELECT value FROM deploy_config WHERE key = 'auto_update_pancrypticon';" | \
                /usr/bin/sqlite3 $CONFIG_DB`
            if [[ $UPDATES_ENABLED == 'False' || $UPDATES_ENABLED == '' ]]; then
                /bin/true
            else
                cd /opt/pancrypticon/pancrypticon && /usr/bin/torsocks /usr/bin/git pull
            fi
        fi && \

        # create settings.env file if missing
        if [ ! -f /opt/pancrypticon/pancrypticon/settings.env ]; then
            /bin/echo " * Generating settings.env..."
            create_env_file /opt/pancrypticon/pancrypticon/settings.env && \
            /bin/echo " * Complete"
        fi

        # if this file is zero size it can cause MariaDB to have trouble
        # starting, so get rid of it
        if [ ! -s /srv/pancrypticon/mariadb/ib_buffer_pool ]; then
            /bin/rm /srv/pancrypticon/mariadb/ib_buffer_pool
        fi

        # tweak memory settings for redis
        if [ $ARCH -eq "amd64" ]; then
            /sbin/sysctl vm.overcommit_memory=1
            /bin/echo "never" > /sys/kernel/mm/transparent_hugepage/enabled
        fi

        # bring up the containers
        if [ -f /opt/pancrypticon/pancrypticon/settings.env ]; then
            compose_cmd="/usr/local/bin/docker-compose --file docker-compose.yml"
            build_opts=""
            up_opts="-d --remove-orphans"
            arch_overlay="docker-compose-${ARCH}.yml"
            cd /opt/pancrypticon/pancrypticon && \
            if [ -f ${arch_overlay} ]; then
                compose_cmd="${compose_cmd} --file ${arch_overlay}"
            fi
            if [ $FORCE_REBUILD -eq 1 ]; then
                build_opts="${build_opts} --no-cache"
                up_opts="${up_opts} --force-recreate"
                /bin/echo " * Forcing rebuild"
            fi
            /bin/echo "- Starting docker containers..."
            /bin/systemctl restart docker && \
            /bin/echo " * Building the latest images..."
            /usr/bin/torsocks ${compose_cmd} pull && \
            ${compose_cmd} build ${build_opts} && \
            ${compose_cmd} up ${up_opts} || croak
            /bin/echo " * Complete"
        fi

        hidden_service=`/bin/cat \
            /var/lib/tor/hidden_service/pancrypticon/hostname`

        /usr/bin/printf "\n\n###########################################\n"
        /usr/bin/printf "Launch completed in $SECONDS seconds \n\n"
        /usr/bin/printf "  Web URL: https://${NEXTCLOUD_DOMAIN_NAME}\n"
        /usr/bin/printf "  Tor URL: http://${hidden_service}\n\n"
        /usr/bin/printf "It may take several minutes for your cloud\n"
        /usr/bin/printf "to become available.\n"
        /usr/bin/printf "###########################################\n"

        /bin/echo \
            "UPDATE deploy_config SET value='True' WHERE key='deployed';" | \
            /usr/bin/sqlite3 /home/daygate/daygate/db.sqlite3 && \

        /bin/echo "SUCCESS Launched successfully"
    else
        /bin/echo \
            "ERROR Could not find Volume Group 'pancrypticon'. Aborting" && \
        croak
    fi
fi
