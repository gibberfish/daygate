#!/usr/bin/env bash

LOCAL_SETTINGS="/home/daygate/daygate/daygate/local_settings.py"
CONFIG_DB="/home/daygate/daygate/db.sqlite3"
TIMEZONE=`/bin/echo "SELECT value FROM deploy_config WHERE key = 'timezone';" \
    | /usr/bin/sqlite3 $CONFIG_DB`
LANGUAGE=`/bin/echo "SELECT value FROM deploy_config WHERE key = 'language';" \
    | /usr/bin/sqlite3 $CONFIG_DB`

if [[ -n $TIMEZONE ]]; then
    # change timezone symlink
    SYMLINK=`/bin/ls -l /etc/localtime | /usr/bin/awk -F'-> ' '{print $2}'`
    if [[ ${SYMLINK} != "/usr/share/zoneinfo/${TIMEZONE}" ]]; then
        /bin/ln -sf /usr/share/zoneinfo/$TIMEZONE /etc/localtime
    fi

    # if the TIME_ZONE setting exists in local_settings.py ...
    if `/bin/egrep -q '^TIME_ZONE' ${LOCAL_SETTINGS}`; then
        # if it's not already equal to TIMEZONE ...
        if [[ `/bin/egrep '^TIME_ZONE' ${LOCAL_SETTINGS}` != "TIME_ZONE='${TIMEZONE}'" ]]; then
            # update Django TIME_ZONE setting
            /bin/sed -i "s|^TIME_ZONE\{0,\}=\{0,\}.*|TIME_ZONE=\"${TIMEZONE}\"|g" $LOCAL_SETTINGS
        fi
    else
        # else add it to local_settings.py
        /bin/echo "TIME_ZONE='${TIMEZONE}'" >> $LOCAL_SETTINGS
    fi
fi

if [[ -n $LANGUAGE ]]; then
    # Do the same as above, but for LANGUAGE_CODE
    if `/bin/egrep -q '^LANGUAGE_CODE' ${LOCAL_SETTINGS}`; then
        if [[ `/bin/egrep '^LANGUAGE_CODE' ${LOCAL_SETTINGS}` != "LANGUAGE_CODE='${LANGUAGE}'" ]]; then
            /bin/sed -i "s|^LANGUAGE_CODE\{0,\}=\{0,\}.*|LANGUAGE_CODE=\"${LANGUAGE}\"|g" $LOCAL_SETTINGS
        fi
    else
        /bin/echo "LANGUAGE_CODE='${LANGUAGE}'" >> $LOCAL_SETTINGS
    fi
fi
