#!/bin/bash

## dumps the databases into the initdb.d directory, where they will be
## automatically loaded on container startup if the data volume is missing

if [[ -r /opt/pancrypticon/pancrypticon/settings.env ]]; then
    cd /opt/pancrypticon/pancrypticon
    mysql_password=`/bin/egrep '^MYSQL_ROOT_PASSWORD=' settings.env | /usr/bin/cut -c 21-`

    /usr/bin/docker exec -it mariadb mysqldump -u root -p"${mysql_password}" --all-databases --single-transaction | /bin/gzip -9 > initdb.d/99-nextcloud.sql.gz
fi
