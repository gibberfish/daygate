#!/bin/bash

CONFIG_DB="/home/daygate/daygate/db.sqlite3"
REBOOT=`/bin/echo "SELECT value FROM deploy_config WHERE key = 'power_control_reboot';" | \
    /usr/bin/sqlite3 $CONFIG_DB`

if [[ $REBOOT == 'False' || $REBOOT == '' ]]; then
    exit 0
else
    # If the reboot flag is true, flip it to false and reboot
    if `/bin/echo "UPDATE deploy_config SET value = 'False' WHERE key = 'power_control_reboot';" | \
      /usr/bin/sqlite3 $CONFIG_DB`; then
      /sbin/reboot
    fi
fi
