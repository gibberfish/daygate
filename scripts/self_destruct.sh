#!/usr/bin/env bash

CONFIG_DB="/home/daygate/daygate/db.sqlite3"
DESTROY=`/bin/echo "SELECT value FROM deploy_config WHERE key = 'self_destruct';" | \
    /usr/bin/sqlite3 $CONFIG_DB`
CONFIRM=`/bin/echo "SELECT value FROM deploy_config WHERE key = 'self_destruct_confirm';" | \
    /usr/bin/sqlite3 $CONFIG_DB`

if [[ $DESTROY == 'False' || $DESTROY == '' ]]; then
    exit 0
elif [[ $CONFIRM == "False" || $CONFIRM == '' ]]; then
    exit 0
else
    LVS=`/sbin/lvs --noheading -o lv_name`
    VGS=`/sbin/vgs --noheading -o vg_name`
    PVS=`/sbin/pvs --noheading -o pv_name`

    /bin/echo "Stopping docker containers..."
    /bin/systemctl stop docker

    /bin/echo "Unmounting volumes..."
    /bin/umount -f /srv /srv /opt

    /bin/echo "Turning off swap..."
    /sbin/swapoff /dev/pancrypticon/swap

    /bin/echo "Removing LVS..."
    for lv in $LVS; do
      /sbin/lvremove -f $lv
    done

    /bin/echo "Removing VGS..."
    for vg in $VGS; do
      /sbin/vgchange -a n $vg
      /sbin/vgremove -f $vg
    done

    /bin/echo "Locking PVS (and throwing away the key)..."
    for pv in $PVS; do
      /sbin/cryptsetup close $pv
      /sbin/cryptsetup erase $pv
    done

    /bin/echo "Shredding PVS..."
    for pv in $PVS; do
      /usr/bin/shred -v $pv &
    done

    # toggle off settings to prevent this from looping forever
    /bin/echo "UPDATE deploy_config SET value = 'False' WHERE key = 'self_destruct'" | \
        /usr/bin/sqlite3 $CONFIG_DB
    /bin/echo "UPDATE deploy_config SET value = 'False' WHERE key = 'self_destruct_confirm'" | \
        /usr/bin/sqlite3 $CONFIG_DB
fi
