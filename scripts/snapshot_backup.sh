#!/bin/bash

MOUNTPOINT='/mnt/snapshot'
DATA_VOL='/dev/mapper/pancrypticon-srv'
SNAPSHOT_VOL='/dev/mapper/pancrypticon-snap'
SNAPSHOT_SIZE=`/sbin/vgdisplay pancrypticon | /bin/grep 'Free  PE' | /usr/bin/awk '{print $5}'`
LOCKFILE='/tmp/snapshot_backup.lock'
CONFIG_DB="/home/daygate/daygate/db.sqlite3"
BACKUP_METHOD=`/bin/echo "SELECT value FROM deploy_config WHERE key = 'backup_method';" | \
    /usr/bin/sqlite3 $CONFIG_DB`
USE_TOR=`/bin/echo "SELECT value FROM deploy_config WHERE key = 'backup_use_tor';" | \
    /usr/bin/sqlite3 $CONFIG_DB`
WEEKDAY=`/bin/date +%w`
DATESTAMP=`/bin/date +%Y%m%d%H%M`

# no-op if backup_method unset or set to 'none'
if [[ $BACKUP_METHOD == '' || $BACKUP_METHOD == 'none' ]]; then
    exit 0
fi

# tell lvm to STFU
export LVM_SUPPRESS_FD_WARNINGS=1

# unmount any existing snapshots
if `/bin/mountpoint -q $MOUNTPOINT`; then
    /bin/echo "Dismounting old snapshot..."
    /bin/umount $MOUNTPOINT
fi

# remove any existing snapshots
if [ -b $SNAPSHOT_VOL ]; then
    /bin/echo "Removing old snapshot..."
    /sbin/lvremove -f $SNAPSHOT_VOL
fi

# create the snapshot
/bin/mkdir -p $MOUNTPOINT
/bin/cp /opt/pancrypticon/pancrypticon/settings.env /srv/pancrypticon/
/bin/cp /home/daygate/daygate/db.sqlite3 /srv/pancrypticon/
cd /opt/pancrypticon/pancrypticon && \
/usr/local/bin/docker-compose stop mariadb redis
/bin/echo "Creating new snapshot..."
/sbin/lvcreate -l+$SNAPSHOT_SIZE -s -n snap $DATA_VOL && \
/usr/local/bin/docker-compose up -d mariadb redis && \
/bin/mount -o nouuid $SNAPSHOT_VOL $MOUNTPOINT

# if ssh is enabled, do the needful
if [[ $BACKUP_METHOD == 'ssh' ]]; then
    # to torify or not to torify...
    if [[ $USE_TOR == 'True' ]]; then
        rsync_cmd='/usr/bin/torsocks /usr/bin/rsync'
    else
        rsync_cmd='/usr/bin/rsync'
    fi

    RSYNC_TARGET=`/bin/echo "SELECT value FROM deploy_config WHERE key = 'backup_ssh_target';" | \
        /usr/bin/sqlite3 $CONFIG_DB`

    /bin/echo "Syncing snapshot..." && \
    $rsync_cmd -aHS -e "/usr/bin/ssh -o StrictHostKeyChecking=no -i /root/.ssh/backups" \
    -M--fake-super --delete --stats $MOUNTPOINT/* $RSYNC_TARGET
# if cifs is enabled, do a poor-man's rsync using tar to preserve file ownership
elif [[ $BACKUP_METHOD == 'cifs' ]]; then
    if [[ $USE_TOR == 'True' ]]; then
        mount_cifs='/usr/bin/torsocks /sbin/mount.cifs'
    else
        mount_cifs='/sbin/mount.cifs'
    fi

    CIFS_MOUNT='/mnt/cifs'
    CIFS_TARGET=`/bin/echo "SELECT value FROM deploy_config WHERE key = 'backup_cifs_target';" | \
        /usr/bin/sqlite3 $CONFIG_DB`
    CIFS_USERNAME=`/bin/echo "SELECT value FROM deploy_config WHERE key = 'backup_cifs_username';" | \
        /usr/bin/sqlite3 $CONFIG_DB`
    CIFS_PASSWORD=`/bin/echo "SELECT value FROM deploy_config WHERE key = 'backup_cifs_password';" | \
        /usr/bin/sqlite3 $CONFIG_DB`
    CIFS_DOMAIN=`/bin/echo "SELECT value FROM deploy_config WHERE key = 'backup_cifs_domain';" | \
        /usr/bin/sqlite3 $CONFIG_DB`

    /bin/echo "Mounting Windows (CIFS) share ${CIFS_TARGET}..."
    /bin/mkdir -p $CIFS_MOUNT && \
    # mount the CIFS share
    $mount_cifs $CIFS_TARGET $CIFS_MOUNT -o username=$CIFS_USERNAME,password=$CIFS_PASSWORD,domain=$CIFS_DOMAIN && \

    # if it's Sunday, or there isn't a tar snapshot file, do a full backup
    if [[ $WEEKDAY == '0' ]] || [[ ! -f $CIFS_MOUNT/snapshot.file ]]; then
        /bin/echo "Creating a full backup..."
        extra_args="--level=0"
        level="full"
    # else do an incremental backup
    else
        /bin/echo "Creating an incremental backup..."
        extra_args=''
        level="incremental"
    fi

    # tar up the LVM snapshot
    cd $MOUNTPOINT && \
    /bin/tar --listed-incremental=$CIFS_MOUNT/snapshot.file $extra_args -czpf $CIFS_MOUNT/gibberfish-backup-$DATESTAMP-$level.tar.gz . && \

    # if this was a full backup, remove previous full backups and incrementals
    if [[ $level == "full" ]]; then
        /bin/echo "Removing expired archives..."
        /bin/rm $CIFS_MOUNT/gibberfish-backup-*-incremental.tar.gz
        for f in `ls $CIFS_MOUNT/gibberfish-backup-*-full.tar.gz`; do
            if [[ $f != "$CIFS_MOUNT/gibberfish-backup-$DATESTAMP-$level.tar.gz" ]]; then
                /bin/rm $f
            fi
        done
    fi
    cd - && \
    /bin/echo "Unmounting Windows (CIFS) share ${CIFS_TARGET}..."
    /bin/umount $CIFS_MOUNT
fi

# umount and remove snapshot
/bin/echo
/bin/echo "Cleaning up..."
/bin/umount $MOUNTPOINT && \
/sbin/lvremove -f $SNAPSHOT_VOL
/bin/rm $LOCKFILE
