#!/usr/bin/env bash

CONFIG_DB="/home/daygate/daygate/db.sqlite3"
LOGDIR="/home/daygate/daygate/webroot/logs"

function get_config {
  /bin/echo -n `/bin/echo "SELECT value FROM deploy_config WHERE key = \
    '$1';" | /usr/bin/sqlite3 $CONFIG_DB`
}

function set_config {
  key=`/bin/echo "SELECT key FROM deploy_config WHERE key = '$1';" | \
    /usr/bin/sqlite3 $CONFIG_DB`
  if [[ -z $key ]]; then
    statement="INSERT into deploy_config (key, value) VALUES('$1', '$2');"
  else
    statement="UPDATE deploy_config SET value='$2' WHERE key='$1';"
  fi
  `/bin/echo "$statement" | /usr/bin/sqlite3 $CONFIG_DB`
}

function is_unlocked {
  /bin/mountpoint -q /opt
}

function docker_status {
  cd /opt/pancrypticon/pancrypticon
  while read -r line; do
    if [ -z "$list" ]; then
      list="$line"
    else
      list="${list}|${line}"
    fi
  done <<< `/usr/bin/docker ps --format "table {{.ID}}:{{.Names}}:{{.Status}}" | \
    /bin/egrep '^[a-z0-9]'`
  /bin/echo "$list"
}

function mounted_filesystems {
  while read -r line; do
    if [ -z "$list" ]; then
      list="$line"
    else
      list="${list}|${line}"
    fi
  done <<< `/bin/df --local --human-readable -x overlay -x shm -x tmpfs \
    -x devtmpfs | /usr/bin/tail -n +2`
  /bin/echo "$list"
}

function memory_usage {
  while read -r line; do
    if [ -z "$list" ]; then
      list="$line"
    else
      list="${list}|${line}"
    fi
  done <<< `/usr/bin/free --human --total | /usr/bin/tail -n +2`
  /bin/echo "$list"
}

function daygate_commit {
  cd /home/daygate/daygate
  /usr/bin/git rev-parse --verify --short HEAD
}

function pancrypticon_commit {
  cd /opt/pancrypticon/pancrypticon
  /usr/bin/git rev-parse --verify --short HEAD
}

function copy_logs {
  /bin/mkdir -p ${LOGDIR}
  for dir in `ls /opt/pancrypticon/docker/containers/| /bin/egrep '^[a-z0-9]{64}$'`; do
    short_id=`/bin/echo $dir |  /usr/bin/cut -c -12`
    logfile="/opt/pancrypticon/docker/containers/${dir}/${dir}-json.log"
    checksum=`/usr/bin/md5sum ${logfile} | /usr/bin/awk '{print $1}'`
    if [ -e $logfile ]; then
      new_log="${LOGDIR}/${short_id}.json.txt"
      if [ -e $new_log ]; then
        if [[ "${checksum}" == `/usr/bin/md5sum ${logfile} | /usr/bin/awk '{print $1}'` ]]; then
          break
        fi
      fi
      /bin/cp $logfile $new_log
      /bin/chown daygate.daygate $new_log
    fi
  done
}

if is_unlocked; then
  set_config "status_locked" "False"
else
  set_config "status_locked" "True"
fi
if [[ "$(get_config 'deployed')" == 'True' ]]; then
  set_config "status_pancrypticon_commit" "$(pancrypticon_commit)"
fi
set_config "status_daygate_commit" "$(daygate_commit)"
set_config "status_docker" "$(docker_status)"
set_config "status_filesystems" "$(mounted_filesystems)"
set_config "status_uptime" "$(/usr/bin/uptime)"
set_config "status_memory" "$(memory_usage)"
set_config "status_updated" "$(/bin/date +%s)"
copy_logs
