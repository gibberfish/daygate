#!/bin/bash

CONFIG_DB="/home/daygate/daygate/db.sqlite3"
UPDATES_ENABLED=`/bin/echo "SELECT value FROM deploy_config WHERE key = 'auto_update_daygate';" | \
    /usr/bin/sqlite3 $CONFIG_DB`

if [[ $UPDATES_ENABLED == 'False' || $UPDATES_ENABLED == '' ]]; then
    exit 0
fi

cd /home/daygate/daygate && \

# check for updates
/usr/bin/torsocks /usr/bin/git remote update 2>&1 > /dev/null
if [[ `/usr/bin/torsocks /usr/bin/git status --untracked-files=no 2>&1 | \
        /bin/grep -c "Your branch is behind"` -eq "1" \
]];
then
     /usr/local/bin/daygate_exec.sh scripts/install.sh $@ || exit $?
fi
